-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2020 at 07:33 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cliniparts`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wc_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wc_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wc_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `we_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `we_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `win_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `win_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image6` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vision_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vision_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mission_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mission_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `goal_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `goal_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `you_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `you_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image7` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fast_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fast_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quality_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quality_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secure_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secure_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `money_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `money_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `easy_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `easy_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `support_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `support_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `image1`, `image2`, `title1`, `title2`, `image3`, `wc_title1`, `wc_title2`, `wc_des`, `we_title`, `we_des`, `win_title`, `win_des`, `image4`, `image5`, `image6`, `vision_title`, `vision_des`, `mission_title`, `mission_des`, `goal_title`, `goal_des`, `you_title`, `you_des`, `image7`, `fast_title`, `fast_des`, `quality_title`, `quality_des`, `secure_title`, `secure_des`, `money_title`, `money_des`, `easy_title`, `easy_des`, `free_title`, `free_des`, `support_title`, `support_des`, `created_at`, `updated_at`) VALUES
(1, 'uFnTvxeuKZ42755hyl0OvHU0ty9CdCLk7kGdOc1a.jpeg', NULL, 'About Us', 'About Us', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-15 12:56:33');

-- --------------------------------------------------------

--
-- Table structure for table `acute_cares`
--

CREATE TABLE `acute_cares` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `acute_cares`
--

INSERT INTO `acute_cares` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, 'jkLpj9gGbCFxkatiVVxjXOD4Gl3d9nkdWGTCiryE.jpeg', NULL, 'Acute Care', 'Acute Care', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-15 12:48:52');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `phone`, `address`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'pabel@gmail.com', '$2y$10$OegEaoN1CXP8d8h7M.NO/.PyQXiqOxUVV6O3ukNyQJyyGFxz.ivta', NULL, NULL, '1', NULL, '2020-07-15 11:49:05'),
(2, 'Pabel', 'tafiq@gmail.com', '$2y$10$zjO1cJ5.D0vQNaIcnIjIUeu1tY3KbS70URtERcYZNsXAXC0LjBdyC', '01823710655', NULL, '2', '2020-07-08 11:40:37', '2020-07-08 11:40:37');

-- --------------------------------------------------------

--
-- Table structure for table `assisteds`
--

CREATE TABLE `assisteds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assisteds`
--

INSERT INTO `assisteds` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Assisted Living', 'Assisted Living', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-13 11:22:12');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `category_id`, `author_id`, `image`, `title`, `des`, `status`, `created_at`, `updated_at`) VALUES
(3, 3, 1, '5fuXIDHcNvaTpOhoPQ4X0E56vgL4MtNGZQb8Gt20.jpeg', 'Math Indroduction', '<p>fahsdlfjkf lakjdfh&nbsp; alskdfh alkdfh</p>\r\n\r\n<p>&nbsp;</p>', 1, '2020-07-26 11:29:42', '2020-07-26 11:29:42'),
(4, 4, 1, 'Sb0sPDgBZCSsrjmaMGCynReO9ravy3Q0OP3ZHBvF.png', 'Education for Nation', 'jhsadf al;sfdj ;a;lfsdkj a;lijfd a;lsdfj', 1, '2020-07-26 11:58:09', '2020-07-26 11:58:09');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Demo', 1, '2020-07-13 12:50:48', '2020-07-13 12:50:48'),
(4, 'Demo2', 1, '2020-07-13 12:51:07', '2020-07-13 12:51:07');

-- --------------------------------------------------------

--
-- Table structure for table `blog_titles`
--

CREATE TABLE `blog_titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_titles`
--

INSERT INTO `blog_titles` (`id`, `image1`, `image2`, `title1`, `title2`, `title3`, `image3`, `created_at`, `updated_at`) VALUES
(1, 'T2Ey96ulZj41TaQzjmm4Ydr58Nb7D84we4zOr7H4.jpeg', NULL, 'Blog Title', 'Blog Title', 'Categories', NULL, NULL, '2020-07-13 12:02:28');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active, 0=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Batteries', 1, '2020-07-18 11:57:18', '2020-07-23 14:40:41'),
(3, 'Electronic', 1, '2020-07-18 11:57:24', '2020-07-23 14:40:16');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_des` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_des` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_des` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `title3`, `title4`, `title5`, `address`, `address_des`, `phone`, `phone_des`, `web`, `web_des`, `created_at`, `updated_at`) VALUES
(1, 'Slx83v8loQqYMRmwVEPHGP0xdGCrkqIph0Erj94l.jpeg', 'mHASwfXzPSa4RQjy1Xta8rEE21xfNvgz37z74BI6.jpeg', 'Contact Us', 'Contact Us', 'Hi, Howdy Let’s Connect us', 'Contact Us', 'Leave us a message', 'All Store Location', 'Address', 'Demo\r\nDemo', 'Phone', 'Demo \r\nDemo', 'Web', 'Demo \r\nDemo', NULL, '2020-07-16 16:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Education & Research', 'Education & Research', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-13 11:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_healths`
--

CREATE TABLE `home_healths` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_healths`
--

INSERT INTO `home_healths` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Home Health & Hospice', 'Home Health & Hospice', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-13 11:19:41');

-- --------------------------------------------------------

--
-- Table structure for table `home_images`
--

CREATE TABLE `home_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image6` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image7` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image8` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image9` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image10` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image11` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image12` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image13` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image14` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_images`
--

INSERT INTO `home_images` (`id`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`, `image7`, `image8`, `image9`, `image10`, `image11`, `image12`, `image13`, `image14`, `created_at`, `updated_at`) VALUES
(1, 'q6UCljdqZixe8SmckQjsk1WVjuM1B8fSE3w6QN2o.jpeg', 'SsAsvxkP4CdIXtcnXZ8PeojZlGrfBNOviF6dkS7u.jpeg', 'Bx5iVCyib8DztjDQehX5Bh5boreAP9vZy7KUng3m.png', '3o10vMIS0OA6b7n6AHFQePg7sbUvwqPxivm0TGVB.png', 'QFDdbVFQrdpc81wbY0B9hUgWk0OfWEfMy5RW4XJE.png', 'ikAHOzaEaldRYGXX2m1bOVZBF6WO6drt8MOWni2T.png', '3WTYZQXVc3qmBC486H6LRlHC7029fJBim93gWCPM.png', '0hzc8P1MvkDAJz9LbtiMNsRmoKVqjNMyX0D3zINu.png', 'C42E0Rl9hIK7zNDjxdxmMkVJrMOItCKsyYsLmIuF.jpeg', 'HksBe5NtTTRHmSKZI9Efnbxhv33QhMlK0LAlJ5Ze.jpeg', 'v9m0WlzhCBNuVXYd23rSg98iExrLwVqyyu2Z5nsE.jpeg', '7coYtnrWWwaNqjDaSLnXaBG74dfUBGs2dePZNr3f.jpeg', 'O657cUC1pkq66spZXc6gWXEghLvBjgUCtvD7SVlD.jpeg', 'iuioe46qnzGSmfjygAMT79XNhDtVMDH9psqLSV4X.png', NULL, '2020-07-23 14:35:58');

-- --------------------------------------------------------

--
-- Table structure for table `home_sections`
--

CREATE TABLE `home_sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active, 0=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_sections`
--

INSERT INTO `home_sections` (`id`, `image`, `title`, `des`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Xcj8L9lHLzLCyzftunwGnXDmnKlHZMl1BODNXIVc.png', 'SECURE PAYMENTS', 'Payment Security', 1, '2020-07-23 14:22:47', '2020-07-23 14:29:56'),
(4, 'GGDSYEh9Z48xa6JpJ4qOsKZud1s5teH1RrNnieTH.png', 'MONEY BACK GUARANTEE', 'Back within 15 days', 1, '2020-07-23 14:25:03', '2020-07-23 14:29:47'),
(5, 'TPX6r2tSUDOaqp3Kgz1DGiUSuX1YFmuLiqeV1XTu.png', 'FREE SHIPPING', 'Start from $100', 1, '2020-07-23 14:25:35', '2020-07-23 14:29:27');

-- --------------------------------------------------------

--
-- Table structure for table `home_texts`
--

CREATE TABLE `home_texts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_texts`
--

INSERT INTO `home_texts` (`id`, `name1`, `name2`, `name3`, `name4`, `name5`, `name6`, `name7`, `name8`, `name9`, `contact`, `created_at`, `updated_at`) VALUES
(1, 'Track Your Order', 'Shop by Device', 'FEATURED ITEMS', 'BEST SELLERS', 'NEW ARRIVAL', 'CONTACT INFO', 'CATEGORIES', 'INFORMATION', 'ABOUT US', '<p>AddressYou address will be here<br />\r\nLorem Ipsum text</p>\r\n\r\n<p>Phone&nbsp;<a href=\"tel:01234567890\">01234 567 890</a></p>\r\n\r\n<p>Web&nbsp;<a href=\"#\">www.example.com</a></p>', NULL, '2020-07-14 12:27:04');

-- --------------------------------------------------------

--
-- Table structure for table `independents`
--

CREATE TABLE `independents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `independents`
--

INSERT INTO `independents` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, 'hzcI8TzCXBCTlw0Ahx5LJiu7wxnSrZCifHph9TVI.jpeg', NULL, 'Independent Service & Organizations', 'Independent Service & Organizations', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-13 11:25:14');

-- --------------------------------------------------------

--
-- Table structure for table `integrateds`
--

CREATE TABLE `integrateds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `integrateds`
--

INSERT INTO `integrateds` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, 'Efq69UITsvWpIpL7J0iASG6gSqi9mVOnsAnnAN4L.jpeg', '6YRa8OsLTIR585xOSWljKF22nElvhA2hDOHtrl9I.jpeg', 'Integrated Delivery Network', 'Integrated Delivery Network', '<h2>YOU CAN&nbsp;CHOOSE US</h2>\r\n\r\n<h1>BECAUSE&nbsp;WE ALWAYS PROVIDE&nbsp;IMPORTANCE...</h1>', 'Clini Parts provide how all this mistaken idea of denouncing pleasure and sing pain was born an will give you a complete account of the system, and expound the actual teachings of the eat explorer of the truth, the mer of human\r\n\r\nClini Parts provide how all this mistaken idea of denouncing pleasure and sing pain was born an will give you a complete account of the system, and expound the actual teachings of the eat explorer of the truth, the mer of human', 'q5hEq3oEbZTnGJYYRP8AVIfSkAuT4wY56b7j6J7K.jpeg', 'FAST DELIVERY', 'Clini Parts provide how all this mistaken dea of denouncing pleasure and sing', 'QUALITY PRODUCT', 'Clini Parts provide how all this mistaken dea of denouncing pleasure and sing', 'SECURE PAYMENT', 'Clini Parts provide how all this mistaken dea of denouncing pleasure and sing', 'MONEY BACK GUARNTEE', 'Clini Parts provide how all this mistaken dea of denouncing pleasure and sing', 'EASY ORDER TRACKING', 'Clini Parts provide how all this mistaken dea of denouncing pleasure and sing', '6PfH2lOTOj5JA0H18n7BUdxMlWkD6aCi7pAI0yCd.jpeg', NULL, '2020-07-23 15:53:13');

-- --------------------------------------------------------

--
-- Table structure for table `long_terms`
--

CREATE TABLE `long_terms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `long_terms`
--

INSERT INTO `long_terms` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, 'eCjdMdyT6zOVw92hMn8RyWpJTFUaQN0aiOK6RkLu.jpeg', NULL, 'Long Term Care', 'Long Term Care', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-13 11:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `manageds`
--

CREATE TABLE `manageds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `manageds`
--

INSERT INTO `manageds` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Managed Care', 'Managed Care', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-13 11:23:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_07_08_170754_create_admins_table', 1),
(5, '2020_07_09_182936_create_sliders_table', 2),
(6, '2020_07_09_193239_create_home_texts_table', 3),
(7, '2020_07_10_043725_create_home_images_table', 4),
(8, '2020_07_10_173628_create_home_sections_table', 5),
(10, '2020_07_11_175843_create_pop_ups_table', 6),
(12, '2020_07_11_181905_create_integrateds_table', 7),
(13, '2020_07_12_025728_create_acute_cares_table', 8),
(14, '2020_07_12_173210_create_surgeries_table', 9),
(15, '2020_07_12_174454_create_long_terms_table', 10),
(16, '2020_07_12_175348_create_education_table', 11),
(17, '2020_07_12_175407_create_home_healths_table', 11),
(18, '2020_07_12_175427_create_physicians_table', 11),
(19, '2020_07_12_175455_create_assisteds_table', 11),
(20, '2020_07_12_175516_create_manageds_table', 11),
(21, '2020_07_12_175534_create_independents_table', 11),
(22, '2020_07_13_163023_create_tech_tips_table', 12),
(23, '2020_07_13_175304_create_blog_titles_table', 13),
(24, '2020_07_13_180410_create_blog_categories_table', 14),
(25, '2020_07_13_182943_create_blogs_table', 15),
(26, '2020_07_14_172620_create_news_titles_table', 16),
(27, '2020_07_14_174110_create_news_categories_table', 17),
(28, '2020_07_14_175354_create_news_table', 18),
(34, '2020_07_15_180200_create_about_us_table', 19),
(35, '2020_07_16_215051_create_contacts_table', 20),
(36, '2020_07_16_222425_create_store_locations_table', 21),
(38, '2020_07_18_171609_create_categories_table', 22),
(39, '2020_07_18_173841_create_sub_categories_table', 23),
(40, '2020_07_18_180204_create_sub_sub_categories_table', 24),
(51, '2020_07_19_173542_create_products_table', 25),
(52, '2020_07_29_181433_create_product_colors_table', 25),
(53, '2020_08_05_181056_add_some_to_users_table', 26),
(54, '2020_08_11_184209_create_subscribes_table', 27);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `category_id`, `author_id`, `image`, `title`, `des`, `status`, `created_at`, `updated_at`) VALUES
(3, 2, 1, 'dEiVfA2eaBS6NCp9JXB6XEQ72903boYttaB02CAZ.png', 'Education for Nation', '<p>asdfasdfasdf asdfkjhlkasdfiahsdf iuahsfdiuahsdf</p>', 1, '2020-07-26 12:15:57', '2020-07-26 12:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `news_categories`
--

CREATE TABLE `news_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_categories`
--

INSERT INTO `news_categories` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Demo', 1, '2020-07-14 12:01:54', '2020-07-14 12:01:54'),
(3, 'Demo2', 1, '2020-07-14 12:01:59', '2020-07-14 12:01:59');

-- --------------------------------------------------------

--
-- Table structure for table `news_titles`
--

CREATE TABLE `news_titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_titles`
--

INSERT INTO `news_titles` (`id`, `image1`, `image2`, `title1`, `title2`, `title3`, `image3`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'News Room Title', 'News Room Title', NULL, NULL, NULL, '2020-07-14 11:36:47');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `physicians`
--

CREATE TABLE `physicians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `physicians`
--

INSERT INTO `physicians` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Physician Offices', 'Physician Offices', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-13 11:20:49');

-- --------------------------------------------------------

--
-- Table structure for table `pop_ups`
--

CREATE TABLE `pop_ups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pop_ups`
--

INSERT INTO `pop_ups` (`id`, `image`, `title1`, `title2`, `place`, `button`, `des`, `created_at`, `updated_at`) VALUES
(1, 'wKogmAHpdO2H0LQYYK3A8BXgC4iIWM1Ok8u54a7C.png', 'SUBSCRIBE OUR NEWSLETTER', 'Get latest product update...', 'Enter your email here', 'Subscribe', 'Be the first in the by getting special deals and offers send directly to your inbox.', NULL, '2020-07-23 14:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_id` bigint(20) DEFAULT NULL,
  `sub_subcategory_id` bigint(20) DEFAULT NULL,
  `color_id` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_price` decimal(18,2) DEFAULT NULL,
  `price` decimal(18,2) DEFAULT NULL,
  `s_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specification` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active, 0=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `subcategory_id`, `sub_subcategory_id`, `color_id`, `top_title`, `name`, `image1`, `image2`, `image3`, `image4`, `old_price`, `price`, `s_des`, `product_code`, `amount`, `des`, `specification`, `featured`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '[\"2\",\"1\"]', 'Sale', 'Fridge', 'YSz6nK7mxF6S9v4oLUC0htxJYlFsK4sEQbWfGSqn.gif', '4r5c8vXDi91i13QyCZ4RqD2VM6ZU58u6ojoSLjMp.jpeg', 'IygkDaNen17BjXmPBcmctZb8lkcHgbnGITtSw1YP.jpeg', 'eC2fO7ozujnNmjNRrcevqDHzRF9GxQhn12IB6RT0.jpeg', '40000.00', '35000.00', NULL, '123ax', '50', NULL, NULL, 1, 1, 1, 1, '2020-07-29 12:42:47', '2020-08-10 21:27:49'),
(2, 3, 3, '[\"2\",\"1\"]', 'Sale', 'TV', 'ZTdPiqOwQ1onLmjMgHUhdtn3syUnlqYa6gMYnkX1.jpeg', 'ZTdPiqOwQ1onLmjMgHUhdtn3syUnlqYa6gMYnkX1.jpeg', 'ZTdPiqOwQ1onLmjMgHUhdtn3syUnlqYa6gMYnkX1.jpeg', 'ZTdPiqOwQ1onLmjMgHUhdtn3syUnlqYa6gMYnkX1.jpeg', '1200.00', '1100.00', 'Nothing', '123658', '50', '<p>Nothing</p>', '<p>Nothing</p>', 0, 1, NULL, 1, '2020-08-10 13:37:05', '2020-08-10 13:55:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_colors`
--

CREATE TABLE `product_colors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_colors`
--

INSERT INTO `product_colors` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'White', 1, '2020-07-29 12:42:34', '2020-07-29 12:42:34'),
(2, 'Black', 1, '2020-07-29 12:42:41', '2020-07-29 12:42:41');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1 = active, 0 = inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `text`, `button`, `link`, `status`, `created_at`, `updated_at`) VALUES
(2, 'mRTDw2ZoYa1YYy9wPCRj2IyK3eckC8sJwMp06WbO.png', '<h1><strong>HURRY UP!</strong></h1>\r\n\r\n<p>10% OFF</p>\r\n\r\n<h3><strong>With Cliniparts</strong></h3>', 'See more', NULL, 1, '2020-07-09 13:16:39', '2020-07-23 14:33:59'),
(5, 'BxqccPZcgQvHcPUMzS0eXtrOjymH302BGuy2Qggw.png', '<h1><strong><strong>HURRY UP!</strong></strong></h1>\r\n\r\n<p><strong>10% OFF</strong></p>\r\n\r\n<h3><strong><strong>With Cliniparts</strong></strong></h3>', 'See more', NULL, 1, '2020-07-21 00:33:00', '2020-07-23 14:33:49');

-- --------------------------------------------------------

--
-- Table structure for table `store_locations`
--

CREATE TABLE `store_locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE `subscribes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribes`
--

INSERT INTO `subscribes` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'berthopabel@gmail.com', '2020-08-11 12:49:23', '2020-08-11 12:49:23');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1 = active, 0 = inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Extra', 1, '2020-07-18 11:57:37', '2020-07-23 14:41:11'),
(3, 3, 'Electrical', 1, '2020-07-18 12:10:00', '2020-07-23 14:40:55');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_categories`
--

CREATE TABLE `sub_sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1 = active, 0 = inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `subcategory_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 'Fridge', 1, '2020-07-18 12:10:13', '2020-07-23 14:41:36'),
(3, 3, 'TV', 1, '2020-08-10 13:35:32', '2020-08-10 13:35:32');

-- --------------------------------------------------------

--
-- Table structure for table `surgeries`
--

CREATE TABLE `surgeries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_des` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_des5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surgeries`
--

INSERT INTO `surgeries` (`id`, `image1`, `image2`, `title1`, `title2`, `m_title`, `m_des`, `image3`, `s_title1`, `s_des1`, `s_title2`, `s_des2`, `s_title3`, `s_des3`, `s_title4`, `s_des4`, `s_title5`, `s_des5`, `image4`, `created_at`, `updated_at`) VALUES
(1, 'PtIla660TII1xx5GuU21DFtibMXZujpkOMdDbhxQ.jpeg', NULL, 'Surgery Centers', 'Surgery Centers', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-13 11:14:25');

-- --------------------------------------------------------

--
-- Table structure for table `tech_tips`
--

CREATE TABLE `tech_tips` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `des1` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `des2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tech_tips`
--

INSERT INTO `tech_tips` (`id`, `image1`, `image2`, `title1`, `title2`, `image3`, `des1`, `image4`, `des2`, `created_at`, `updated_at`) VALUES
(1, '3HFBTNoU2k7ZATfRwSO8e5xnzhkkQuygXjSaeNlE.jpeg', NULL, 'Tech Tips', 'Tech Tips', NULL, NULL, NULL, NULL, NULL, '2020-07-13 12:01:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_original` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `image`, `company`, `address1`, `address2`, `avatar_original`, `avatar`, `google_id`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Unknown asdf', NULL, 'berthopabel@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$6Gjr.zvNfOl8ivEJ/Nl8qeeJ.AjWoIdvZTGmdYezaxkyYxEGJi.UK', NULL, '2020-08-08 11:36:03', '2020-08-08 11:36:03'),
(2, 'Unknown asdf', 'Unknown asdf', 'admin@gmail.com', '01823710688', '1596910512.jpg', 'berthopabel@gmail.com', 'berthopabel@gmail.com', 'berthopabel@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$BF2He/sO4F8.xhUaRxWDhuPMfA2apavys5PYv9yhA9S6YuYAxLEZi', NULL, '2020-08-08 12:15:12', '2020-08-08 12:15:12'),
(3, 'hanif mia pabel', NULL, 'hanifmiapabel@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '104591905092178145762', NULL, '$2y$10$ihMOYTbjKNF/O0Xig1OwceCDe9Kd0p1NZozZG6LsnGwCv7GkyBJfy', NULL, '2020-08-10 21:40:50', '2020-08-10 21:40:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acute_cares`
--
ALTER TABLE `acute_cares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD UNIQUE KEY `admins_phone_unique` (`phone`);

--
-- Indexes for table `assisteds`
--
ALTER TABLE `assisteds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_category_id_foreign` (`category_id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_titles`
--
ALTER TABLE `blog_titles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_healths`
--
ALTER TABLE `home_healths`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_images`
--
ALTER TABLE `home_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_sections`
--
ALTER TABLE `home_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_texts`
--
ALTER TABLE `home_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `independents`
--
ALTER TABLE `independents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `integrateds`
--
ALTER TABLE `integrateds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `long_terms`
--
ALTER TABLE `long_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manageds`
--
ALTER TABLE `manageds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_category_id_foreign` (`category_id`);

--
-- Indexes for table `news_categories`
--
ALTER TABLE `news_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_titles`
--
ALTER TABLE `news_titles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `physicians`
--
ALTER TABLE `physicians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pop_ups`
--
ALTER TABLE `pop_ups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_colors`
--
ALTER TABLE `product_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_locations`
--
ALTER TABLE `store_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_sub_categories_subcategory_id_foreign` (`subcategory_id`);

--
-- Indexes for table `surgeries`
--
ALTER TABLE `surgeries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tech_tips`
--
ALTER TABLE `tech_tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `acute_cares`
--
ALTER TABLE `acute_cares`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assisteds`
--
ALTER TABLE `assisteds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blog_titles`
--
ALTER TABLE `blog_titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_healths`
--
ALTER TABLE `home_healths`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_images`
--
ALTER TABLE `home_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_sections`
--
ALTER TABLE `home_sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `home_texts`
--
ALTER TABLE `home_texts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `independents`
--
ALTER TABLE `independents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `integrateds`
--
ALTER TABLE `integrateds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `long_terms`
--
ALTER TABLE `long_terms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `manageds`
--
ALTER TABLE `manageds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news_categories`
--
ALTER TABLE `news_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news_titles`
--
ALTER TABLE `news_titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `physicians`
--
ALTER TABLE `physicians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pop_ups`
--
ALTER TABLE `pop_ups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_colors`
--
ALTER TABLE `product_colors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `store_locations`
--
ALTER TABLE `store_locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `surgeries`
--
ALTER TABLE `surgeries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tech_tips`
--
ALTER TABLE `tech_tips`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `blog_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `news_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD CONSTRAINT `sub_sub_categories_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
