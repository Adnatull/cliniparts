function addToCart(id) {
var cartItems = JSON.parse(localStorage.getItem('cartItems'));
if(cartItems !== null && (cartItems[id] !== null && cartItems[id] !== undefined)) {
  alert("The item already in cart");
  return;
}
if(cartItems === null || cartItems === undefined) {
  cartItems = [];
}
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
           type:'GET',
           url: '/add-to-cart/'+id,
           dataType: 'json',
           success:function(data){
              if(data.success) {
                var product =  data.success;
                cartItems[product.id] = product;
                console.log(product);
                localStorage.setItem('cartItems', JSON.stringify(cartItems));
                countCartProducts();
                showProductsInCart()
              } else {
                alert("The product is not currently available!");
              }
           },
        });
}

function countCartProducts() {
  var cartItems = JSON.parse(localStorage.getItem('cartItems'));
  if(cartItems === null || cartItems === undefined) {
    document.getElementById('cartProductsCount').innerHTML= 0;
  } else {
    var length = cartItems.length;
    var cnt = 0;
    for(var i=0; i<length; i++) {
      if(cartItems[i] !== null && cartItems[i] !== undefined) {
        cnt++;
      }
    }
    document.getElementById('cartProductsCount').innerHTML= cnt;
  }
}
$('#cartProductsCount').ready(countCartProducts);

function showCart() {
  console.log('Hello');
  var cartItems = JSON.parse(localStorage.getItem('cartItems'));
}

$('#ProductsCart').ready(showProductsInCart);

function showProductsInCart() {
  var cartItems = JSON.parse(localStorage.getItem('cartItems'));

  if(cartItems === null || cartItems === undefined) {
    document.getElementById('ProductsCart').innerHTML= '';
  } else {
    var length = cartItems.length;
    document.getElementById('ProductsCart').innerHTML= '';
    var form = document.createElement('form');
    form.setAttribute('id', 'CheckOut-Cart');
    form.setAttribute('name', 'CheckOut-Cart');
    form.setAttribute('method', 'POST');
    for(var i=0; i<length; i++) {
      if(cartItems[i] !== null && cartItems[i] !== undefined) {

        var li = document.createElement('li');
        var a_image = document.createElement('a');
        a_image.setAttribute('class', 'image');
        var img = document.createElement('img');
        img.setAttribute('src', 'assets/backend/images/Product/'+cartItems[i].image1);
        img.setAttribute('alt', 'product');
        a_image.appendChild(img);
        li.appendChild(a_image);

        var divMain = document.createElement('div');
        divMain.setAttribute('class', 'content');
        var a = document.createElement('a');
        a.setAttribute('href', 'javascript:void(0)');
        a.setAttribute('class', 'title');
        a.innerHTML = cartItems[i].name;
        divMain.appendChild(a);
        var span = document.createElement('span');
        span.setAttribute('class', 'price');
        span.innerHTML = 'Price: ' + cartItems[i].price;
        divMain.appendChild(span);
        var span = document.createElement('span');
        span.setAttribute('class', 'qty');
        var div = document.createElement('div');
        div.innerHTML = 'Qty: ';
        span.appendChild(div);
        var button = document.createElement('button');
        button.setAttribute('class', 'bg-primary text-white');
        button.setAttribute('type', 'button');
        button.setAttribute('style', 'height: 35px;width: 35px');
        button.setAttribute('onclick', 'decreaseProductQuantity(quantity' + cartItems[i].id +')');
        button.innerHTML = '-';
        span.appendChild(button);

        var input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('name', 'quantity[]')
        input.setAttribute('value', 1);
        input.setAttribute('id', 'quantity' + cartItems[i].id);
        input.setAttribute('class', 'ProductQuantity');

        span.appendChild(input);
        var button = document.createElement('button');
        button.setAttribute('class', 'bg-primary text-white');
        button.setAttribute('type', 'button');
        button.setAttribute('style', 'height: 35px;width: 35px');
        button.setAttribute('onclick', 'increaseProductQuantity(quantity' + cartItems[i].id +')');
        button.innerHTML = '+';
        span.appendChild(button);
        divMain.appendChild(span);
        li.appendChild(divMain);

        var input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('class', 'price');
        input.setAttribute('value', cartItems[i].price);
        input.setAttribute('name', 'price[]');
        li.appendChild(input);

        var input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('id', 'productId');
        input.setAttribute('value', cartItems[i].id);
        input.setAttribute('name', 'productId[]');
        li.appendChild(input);

        var button = document.createElement('button');
        button.setAttribute('class', 'remove');
        button.setAttribute('data-id', cartItems[i].id);
        button.setAttribute('onclick', 'removeItemFromCart('+cartItems[i].id+')');
        var icon = document.createElement('i');
        icon.setAttribute('class', 'fa fa-trash-o');
        button.appendChild(icon);
        li.appendChild(button);
        form.appendChild(li);


      }
    }
    var div = document.createElement('div');
    div.setAttribute('class', 'mini-cart-bottom');
    var h4 = document.createElement('h4');
    h4.setAttribute('class', 'sub-total');
    var divMin = document.createElement('div');
    divMin.innerHTML = "Total: ";
    h4.appendChild(divMin);
    var span = document.createElement('span');
    span.innerHTML = 0;
    h4.appendChild(span);
    div.appendChild(h4);
    var divMin = document.createElement('div');
    divMin.setAttribute('class', 'button');
    var a = document.createElement('a');
    a.innerHTML = 'Check Out';
    divMin.appendChild(a);

    var input = document.createElement('input');
    input.setAttribute('type', 'submit');
    input.setAttribute('name', 'submit');
    input.setAttribute('value', 'Checkout');
    divMin.appendChild(input);
    div.appendChild(divMin);
    form.appendChild(div);
    document.getElementById('ProductsCart').appendChild(form);

  }
}
$(document).ready(function() {
  $("#CheckOut-Cart").submit(function (event) {
      event.preventDefault();

      var data = $(this).serializeArray();

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
               type:'POST',
               url: '',
               data: data,
               success:function(data){
                  if(data.success) {
                      alert("Order Submitted successfully");
                      localStorage.removeItem("cartItems");
                  } else {
                    alert("Something is wrong");
                  }
               },
            });


  });
});


function removeItemFromCart(id) {
  var cartItems = JSON.parse(localStorage.getItem('cartItems'));
  for(var i=0; i<cartItems.length; i++) {
    if(cartItems[i] == null || cartItems[i] == undefined) {
      continue;
    }
    if(cartItems[i].id == id + '') {
      cartItems.splice(i, 1);
      localStorage.setItem('cartItems', JSON.stringify(cartItems));
      countCartProducts();
      showProductsInCart()
      break;
    }
  }
}

function decreaseProductQuantity(product) {
  var num = parseInt(product.value) - 1;
  if(num <= 0) {
    num = 0;
  }
  product.value = num;
}

function increaseProductQuantity(product) {
  var num = parseInt(product.value) + 1;
  product.value = num;
}
