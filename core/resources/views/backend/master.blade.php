<!doctype html>
<html class="no-js" lang="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<link rel="icon" href="{{ asset('assets/backend/img/fevicon.png') }}">
<head>
    <title>Admin || Cliniparts</title>
    @include('backend.partials.meta')
    @include('backend.partials.style')
</head>

<body>
<div id="preloader"></div>
<div id="wrapper" class="wrapper bg-ash">
@include('backend.partials.header')
    <div class="dashboard-page-one">
    @include('backend.partials.sidebar')
        <div class="dashboard-content-one">
            @yield('content')
        </div>
    </div>
</div>
<footer class="footer-wrap-layout1 text-center">
    <div class="copyright">© Copyrights <a href="#">Cliniparts</a> {{ \Carbon\Carbon::now()->format('Y') }}. All rights reserved. Developed
        by <a href="#">4 Devs</a>
    </div>
</footer>
@include('backend.partials.js')
@include('backend.partials.notifications')
@yield('js')
</body>

</html>
