@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>About Us One</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>Acute Care</li>
        </ul>
    </div>
    <div class="card height-auto">
        <div class="card-body">
            <form action="{{ route('update.about.us') }}" method="POST" class="new-added-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($about['image1']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/About/'.$about['image1']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner One</label>
                        <input type="file" name="image1" class="form-control-file" accept="image/">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($about['image2']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/About/'.$about['image2']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner Two</label>
                        <input type="file" name="image2" class="form-control-file" accept="image/">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Title1 <span class="text-danger">*</span></label>
                        <input type="text" name="title1" value="{{ $about['title1'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Title2 <span class="text-danger"></span></label>
                        <input type="text" name="title2" value="{{ $about['title2'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($about['image3']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/About/'.$about['image3']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image</label>
                        <input type="file" name="image3" class="form-control-file" accept="image/">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Welcome Title1 <span class="text-danger"></span></label>
                        <input type="text" name="wc_title1" value="{{ $about['wc_title1'] }}" placeholder="" class="form-control">
                        <input type="hidden" name="id" value="{{ $about['id'] }}">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Welcome Title2 <span class="text-danger"></span></label>
                        <input type="text" name="wc_title2" value="{{ $about['wc_title2'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Welcome Description <span class="text-danger"></span></label>
                        <textarea name="wc_des" placeholder="" class="form-control">{{ $about['wc_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>We Start Title <span class="text-danger"></span></label>
                        <input type="text" name="we_title" value="{{ $about['we_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>We Start Description <span class="text-danger"></span></label>
                        <textarea name="we_des" placeholder="" class="form-control">{{ $about['we_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Win Best Title <span class="text-danger"></span></label>
                        <input type="text" name="win_title" value="{{ $about['win_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Win Best Description <span class="text-danger"></span></label>
                        <textarea name="win_des" placeholder="" class="form-control">{{ $about['win_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($about['image4']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/About/'.$about['image4']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image One</label>
                        <input type="file" name="image4" class="form-control-file" accept="image/">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($about['image5']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/About/'.$about['image5']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image Two</label>
                        <input type="file" name="image5" class="form-control-file" accept="image/">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($about['image6']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/About/'.$about['image6']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image Three</label>
                        <input type="file" name="image6" class="form-control-file" accept="image/">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Our Vision Title <span class="text-danger"></span></label>
                        <input type="text" name="vision_title" value="{{ $about['vision_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Our Vision Description <span class="text-danger"></span></label>
                        <textarea name="vision_des" placeholder="" class="form-control">{{ $about['vision_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Our Mission Title <span class="text-danger"></span></label>
                        <input type="text" name="mission_title" value="{{ $about['mission_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Our Mission Description <span class="text-danger"></span></label>
                        <textarea name="mission_des" placeholder="" class="form-control">{{ $about['mission_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Our Goal Title <span class="text-danger"></span></label>
                        <input type="text" name="goal_title" value="{{ $about['goal_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Our Goal Description <span class="text-danger"></span></label>
                        <textarea name="goal_des" placeholder="" class="form-control">{{ $about['goal_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>You Can Choose Title <span class="text-danger"></span></label>
                        <input type="text" name="you_title" value="{{ $about['you_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>You Can Choose Description <span class="text-danger"></span></label>
                        <textarea name="you_des" placeholder="" class="form-control">{{ $about['you_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($about['image7']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/About/'.$about['image7']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image</label>
                        <input type="file" name="image7" class="form-control-file" accept="image/">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Fast Delivery Title <span class="text-danger"></span></label>
                        <input type="text" name="fast_title" value="{{ $about['fast_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Fast Delivery Description <span class="text-danger"></span></label>
                        <textarea name="fast_des" placeholder="" class="form-control">{{ $about['fast_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Quality Product Title <span class="text-danger"></span></label>
                        <input type="text" name="quality_title" value="{{ $about['quality_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Quality Product Description <span class="text-danger"></span></label>
                        <textarea name="quality_des" placeholder="" class="form-control">{{ $about['quality_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Secure Payment Title <span class="text-danger"></span></label>
                        <input type="text" name="secure_title" value="{{ $about['secure_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Secure Payment Description <span class="text-danger"></span></label>
                        <textarea name="secure_des" placeholder="" class="form-control">{{ $about['secure_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Money Back Title <span class="text-danger"></span></label>
                        <input type="text" name="money_title" value="{{ $about['money_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Money Back Description <span class="text-danger"></span></label>
                        <textarea name="money_des" placeholder="" class="form-control">{{ $about['money_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Easy Order Title <span class="text-danger"></span></label>
                        <input type="text" name="easy_title" value="{{ $about['easy_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Easy Order Description <span class="text-danger"></span></label>
                        <textarea name="easy_des" placeholder="" class="form-control">{{ $about['easy_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Free Return Title <span class="text-danger"></span></label>
                        <input type="text" name="free_title" value="{{ $about['free_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Free Return Description <span class="text-danger"></span></label>
                        <textarea name="free_des" placeholder="" class="form-control">{{ $about['free_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>24/7 Support Title <span class="text-danger"></span></label>
                        <input type="text" name="support_title" value="{{ $about['support_title'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>24/7 Support Description <span class="text-danger"></span></label>
                        <textarea name="support_des" placeholder="" class="form-control">{{ $about['support_des'] }}</textarea>
                    </div>

                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
@endsection
