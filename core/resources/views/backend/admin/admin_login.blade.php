<!DOCTYPE HTML>
<html>
<head>
    <title>Admin Login :: Cliniparts</title>
    @include('backend.partials.meta')
    @include('backend.partials.style')
</head>

<body style="background-color: white;">
<!-- PAGE CONTENT -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="login card auth-box mx-auto my-auto mt-5">
                        <div class="card-block text-center">
                            <div class="user-icon">
                                <h3 style="margin-top: 50px;">Cliniparts</h3>
{{--                                <img src="{{ asset('assets/backend/img/ecommerce.png') }}" height="70" width="100">--}}
                            </div>
                            <h4 class="text-danger">Admin Login</h4>
                            <div class="user-details">
                                <form  action="{{ route('admin.loginCheck') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">
                                                <i class="fa fa-user-o"></i>
                                            </span>
                                            <input type="email" class="form-control" name="email" placeholder="Email" aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">
{{--                                                <i class="fa fa-key"></i>--}}
                                            </span>
                                            <input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
</div>
<!-- /PAGE CONTENT -->
@include('backend.partials.js')
@include('backend.partials.notifications')
</body>

</html>
