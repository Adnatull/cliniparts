@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>News Room Title</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>News Room Title</li>
        </ul>
    </div>
    <div class="card height-auto">
        <div class="card-body">
            <form action="{{ route('update.news.title') }}" method="POST" class="new-added-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($title['image1']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/Blog/'.$title['image1']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner One</label>
                        <input type="file" name="image1" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($title['image2']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/Blog/'.$title['image2']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner Two</label>
                        <input type="file" name="image2" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Title1 <span class="text-danger">*</span></label>
                        <input type="text" name="title1" value="{{ $title['title1'] }}" placeholder="" class="form-control">
                        <input type="hidden" name="id" value="{{ $title['id'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Title2 <span class="text-danger">*</span></label>
                        <input type="text" name="title2" value="{{ $title['title2'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Category Title <span class="text-danger">*</span></label>
                        <input type="text" name="title3" value="{{ $title['title3'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($title['image3']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/Blog/'.$title['image3']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image One</label>
                        <input type="file" name="image3" class="form-control-file" accept="image/*">
                    </div>

                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
@endsection
