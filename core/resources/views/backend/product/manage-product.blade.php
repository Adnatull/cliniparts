@extends('backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="card-title mt-2">Product List</h4>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table_id">
                                <thead class="">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Product Color</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Prev. Price</th>
                                    <th scope="col">Product Code</th>
                                    <th scope="col">Add Featured</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($products as $product)
                                    <tr>
                                        <th scope="row">{{ $i++ }}</th>
                                        <td><b>{{ $product->subSubCategoryName->subCategoryName->categoryName['name'] }}
                                        <td><b>{{ $product->name }}
                                        <td>@if($product->image1)
                                            <a target="_blank" href="{{ asset('assets/backend/images/Product/'.$product->image1) }}">
                                                <img src="{{ asset('assets/backend/images/Product/'.$product->image1) }}" style="max-height: 50px;">
                                            </a>@endif
                                        </td>
                                        @if($product->color_id)
                                            <td><b>
                                            @foreach(json_decode($product->color_id, true) as $proId)
                                                @foreach($colors as $col_id)
                                                    @if($proId == $col_id->id)
                                                        {{ $col_id->name }},
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        @endif
                                        <td><b>{{ $product->price }}
                                        <td><b>{{ $product->old_price }}
                                        <td><b>{{ $product->product_code }}
                                        <td>@if($product->featured == 1) <button class="btn-lg btn-success">Done</button>
                                            @else
                                                <a href="{{ route('add.to.featured', ['id' => $product->id]) }}" class="btn-lg btn-secondary mt-2"> Add</a>
                                            @endif
                                        </td>
                                        <td>@if($product->status == 1) <button class="btn btn-success">Active</button> @else <button class="btn  btn-warning">Inactive</button>  @endif
                                            <a href="{{ route('view.product', ['id' => $product->id]) }}" target="_blank" class="btn btn-secondary mt-2"><i class="fa fa-search"></i> View</a>
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="{{ route('edit.product', ['id'=>$product->id]) }}" class="btn btn-primary btn-lg">Edit</a>
                                                <span class="ml-2"></span>
                                                <a href="#deleteProduct-{{ $product->id }}" data-toggle="modal" class="btn btn-danger btn-lg">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="deleteProduct-{{ $product->id }}" class="modal fade">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"><i class="fa fa-trash text-danger" aria-hidden="true"></i></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('delete.product') }}" method="POST">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <p>Are you want to delete this?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="hidden" name="id" value="{{ $product->id }}">
                                                            <button type="submit" class="btn btn-danger btn-lg">Delete</button>
                                                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection
