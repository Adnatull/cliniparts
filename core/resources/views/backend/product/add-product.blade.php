@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>Add Product</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>Product</li>
        </ul>
    </div>

    <div class="card height-auto">
        <div class="card-body">
            <form class="new-added-form" action="{{ route('save.product') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Category <span class="text-danger">*</span></label>
                        <select name="category_id" onchange="getCategory();" id="category_id" class="select2" >
                            <option value=" ">---Select Category---</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Sub Category <span class="text-danger">*</span></label>
                        <select name="subcategory_id" onchange="getSubCategory()" id="subcategory_id" class="select2 subcategory_id" >
                            <option value=" ">---Select First Category---</option>
                        </select>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Sub Sub Category <span class="text-danger">*</span></label>
                        <select name="sub_subcategory_id"  id="sub_subcategory_id" class="select2 sub_subcategory_id" >
                            <option value=" ">---Select First Sub Category---</option>
                        </select>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Top Title <span class="text-danger">*</span></label>
                        <input type="text" name="top_title" value="{{ old('top_title') }}" placeholder="Sale / New" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" value="{{ old('name') }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Main Image <span class="text-danger">*</span></label>
                        <input type="file" name="image1" value="{{ old('image1') }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Image2 <span class="text-danger"></span></label>
                        <input type="file" name="image2" value="{{ old('image2') }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Image3 <span class="text-danger"></span></label>
                        <input type="file" name="image3" value="{{ old('image3') }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Image4 <span class="text-danger"></span></label>
                        <input type="file" name="image4" value="{{ old('image4') }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Product Color <span class="text-danger">*</span></label>
                        <select name="color_id[]" multiple class="select2" >
                            <option value=" ">---Select Color---</option>
                            @foreach($colors as $color)
                                <option value="{{ $color->id }}">{{ $color->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Previous Price <span class="text-danger"></span></label>
                        <input type="number" name="old_price" value="{{ old('old_price') }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Price <span class="text-danger">*</span></label>
                        <input type="number" name="price" value="{{ old('price') }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Product Code <span class="text-danger">*</span></label>
                        <input type="text" name="product_code" value="{{ old('product_code') }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Amount of Products <span class="text-danger"></span></label>
                        <input type="text" name="amount" value="{{ old('amount') }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-lg-6 col-12 form-group">
                        <label>Short Description</label>
                        <textarea class="textarea form-control" name="s_des" cols="10" rows="9">{{ old('s_des') }}</textarea>
                    </div>
                    <div class="col-lg-6 col-12 form-group">
                        <label>Description</label>
                        <textarea class="textarea form-control" name="des"  id="editor" cols="10" rows="9">{{ old('des') }}</textarea>
                    </div>
                    <div class="col-lg-6 col-12 form-group">
                        <label>Specification</label>
                        <textarea class="textarea form-control" name="specification"  id="editor1" cols="10" rows="9">{{ old('specification') }}</textarea>
                    </div>
                    <div class="col-lg-6 col-12 form-group mg-t-30">
                        <label class="control-label">Status</label>
                        <select name="status" class="form-control select2 m-2">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
