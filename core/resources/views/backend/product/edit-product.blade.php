@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>Product</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>Update Product</li>
        </ul>
    </div>
    <div class="card height-auto">
        <div class="card-body">
            <form name="edit_product" action="{{ route('update.product') }}" method="POST" class="new-added-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Category <span class="text-danger">*</span></label>
                        <select name="category_id" onchange="gCategory();" id="c_id" class="select2" >
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Sub Category <span class="text-danger">*</span></label>
                        <select name="subcategory_id" onchange="gSubCategory();" id="sc_id" class="select2 sc_id" >
                            @foreach($subCategories as $subCategory)
                                <option value="{{ $subCategory->id }}">{{ $subCategory->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Sub Sub Category <span class="text-danger">*</span></label>
                        <select name="sub_subcategory_id"  id="ssc_id" class="select2 ssc_id" >
                            @foreach($subSubCategories as $subSubCategory)
                                <option value="{{ $subSubCategory->id }}">{{ $subSubCategory->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" value="{{ $product->name }}" placeholder="" class="form-control">
                        <input type="hidden" name="id" value="{{ $product->id }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Top Title <span class="text-danger">*</span></label>
                        <input type="text" name="top_title" value="{{ $product->top_title }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Price <span class="text-danger">*</span></label>
                        <input type="text" name="price" value="{{ $product->price }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Previous Price <span class="text-danger"></span></label>
                        <input type="text" name="old_price" value="{{ $product->old_price }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Product Code <span class="text-danger">*</span></label>
                        <input type="text" name="product_code" value="{{ $product->product_code }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Available <span class="text-danger"></span></label>
                        <input type="text" name="amount" value="{{ $product->amount }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Product Color <span class="text-danger"></span></label>
                        <select name="color_id[]" multiple id="color_id" class="form-control select2 color_id">
                            @foreach(json_decode($product->color_id, true) as $proId)
                                @foreach($colors as $col_id)
                                    @if($proId == $col_id->id)
                                        <option value="{{ $col_id->id }}" selected>{{ $col_id->name }}</option>
                                    @else
                                        <option value="{{ $col_id->id }}">{{ $col_id->name }}</option>
                                    @endif
                                @endforeach
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-4 col-12 form-group mg-t-30">
                        @if($product->image1)
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/Product/'.$product->image1) }}" style="max-height: 100px;width: 50px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Main Image</label>
                        <input type="file" name="image1" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-lg-4 col-12 form-group mg-t-30">
                        @if($product->image2)
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/Product/'.$product->image2) }}" style="max-height: 100px;width: 50px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image2</label>
                        <input type="file" name="image2" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-lg-4 col-12 form-group mg-t-30">
                        @if($product->image3)
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/Product/'.$product->image3) }}" style="max-height: 100px;width: 50px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image3</label>
                        <input type="file" name="image3" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-lg-6 col-12 form-group mg-t-30">
                        @if($product->image4)
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/Product/'.$product->image4) }}" style="max-height: 100px;width: 50px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image4</label>
                        <input type="file" name="image4" class="form-control-file" accept="image/*">
                    </div>

                    <div class="col-lg-6 col-12 form-group">
                        <label>Short Description</label>
                        <textarea class="textarea form-control" name="s_des"  id="form-message" cols="10" rows="9">{{ $product->s_des }}</textarea>
                    </div>

                    <div class="col-lg-6 col-12 form-group">
                        <label>Description</label>
                        <textarea class="textarea form-control" name="des"  id="editor" cols="10" rows="9">{{ $product->des }}</textarea>
                    </div>

                    <div class="col-lg-6 col-12 form-group">
                        <label>Specification</label>
                        <textarea class="textarea form-control" name="specification"  id="editor1" cols="10" rows="9">{{ $product->specification }}</textarea>
                    </div>

                    <div class="col-lg-6 col-12 form-group mg-t-30">
                        <label class="control-label">Status</label>
                        <select name="status" class="form-control select2 m-2">
                            @if($product->status == 1)
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            @else
                                <option value="0">Inactive</option>
                                <option value="1">Active</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script>
        document.forms['edit_product'].elements['category_id'].value = '{{ $product->subSubCategoryName->subCategoryName->categoryName['id'] }}';
        document.forms['edit_product'].elements['subcategory_id'].value = '{{ $product->subSubCategoryName->subCategoryName['id'] }}';
        document.forms['edit_product'].elements['sub_subcategory_id'].value = '{{ $product->subSubCategoryName['id'] }}';
        document.forms['edit_product'].elements['color_id'].value = '{{ $product['color_id'] }}';
    </script>
@endsection
