@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>Home Page Text</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>Home Page Text</li>
        </ul>
    </div>
    <div class="card height-auto">
        <div class="card-body">
            <form action="{{ route('update.home.text') }}" method="POST" class="new-added-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Track Order <span class="text-danger">*</span></label>
                        <input type="text" name="name1" value="{{ $homeText['name1'] }}" placeholder="" class="form-control">
                        <input type="hidden" name="id" value="{{ $homeText['id'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Device Search <span class="text-danger">*</span></label>
                        <input type="text" name="name2" value="{{ $homeText['name2'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Featured Items <span class="text-danger">*</span></label>
                        <input type="text" name="name3" value="{{ $homeText['name3'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Best Sellers <span class="text-danger">*</span></label>
                        <input type="text" name="name4" value="{{ $homeText['name4'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>New Arrival <span class="text-danger">*</span></label>
                        <input type="text" name="name5" value="{{ $homeText['name5'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Footer Title One <span class="text-danger">*</span></label>
                        <input type="text" name="name6" value="{{ $homeText['name6'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Footer Title Two <span class="text-danger">*</span></label>
                        <input type="text" name="name7" value="{{ $homeText['name7'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Footer Title Three <span class="text-danger">*</span></label>
                        <input type="text" name="name8" value="{{ $homeText['name8'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Footer Title Four <span class="text-danger">*</span></label>
                        <input type="text" name="name9" value="{{ $homeText['name9'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-12 form-group">
                        <label>Contact Information <span class="text-danger">*</span></label>
                        <textarea name="contact" id="editor" cols="60" rows="5">{{ $homeText['contact'] }}</textarea>
                    </div>

                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
@endsection
