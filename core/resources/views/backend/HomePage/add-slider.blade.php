@extends('backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="card-title mt-2">Add Slider</h4>
                    <p class="text-right">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addSlider">Add Slider</button>
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table_id">
                                <thead class="">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Text</th>
                                    <th scope="col">Button</th>
                                    <th scope="col">Link</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($sliders as $slider)
                                    <tr>
                                        <th scope="row">{{ $i++ }}</th>
                                        <td>@if($slider->image)
                                                <a target="_blank" href="{{ asset('assets/backend/images/HomePage/'.$slider->image) }}">
                                                    <img src="{{ asset('assets/backend/images/HomePage/'.$slider->image) }}" style="max-height: 50px;">
                                                </a>@endif
                                        </td>
                                        <td><b>{!! $slider->text !!}
                                        <td><b>{{ $slider->button }}
                                        <td><b>{{ $slider->link }}
                                        <td><b>@if($slider->status == 1) <button class="btn btn-success">Active</button> @else <button class="btn btn-warning">Inactive</button>  @endif</td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="#editSlider-{{ $slider->id }}" data-toggle="modal" class="btn btn-primary btn-lg">Edit</a>
                                                <span class="ml-2"></span>
                                                <a href="#deleteSlider-{{ $slider->id }}" data-toggle="modal" class="btn btn-danger btn-lg">Delete</a>
                                            </div>
                                        </td>
                                    </tr>

                                    <div id="deleteSlider-{{ $slider->id }}" class="modal fade">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"><i class="fa fa-trash text-danger" aria-hidden="true"></i></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('delete.slider') }}" method="POST">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <p>Are you want to delete this?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="hidden" name="id" value="{{ $slider->id }}">
                                                            <button type="submit" class="btn btn-danger btn-lg">Delete</button>
                                                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="editSlider-{{ $slider->id }}" class="modal fade">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Edit Slider</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('update.slider') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        @if(($slider->image))
                                                            <img src="{{ asset('assets/backend/images/HomePage/'.$slider->image) }}" class="m-3" style="max-height: 50px;max-width: 80px">
                                                        @endif
                                                        <div class="form-group">
                                                            <label class="control-label">Image <span class="text-danger">*</span></label>
                                                            <input type="file" name="image" class="form-control" accept="image/*">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Text<span class="text-danger"></span></label>
                                                            <textarea id="editor1" name="text" class="form-control">{{ $slider->text }}</textarea>
                                                            <input type="hidden" name="id" value="{{ $slider->id }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Button Name<span class="text-danger"></span></label>
                                                            <input type="text" name="button" value="{{ $slider->button }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Button Link<span class="text-danger"></span></label>
                                                            <input type="text" name="link" value="{{ $slider->link }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Status</label>
                                                            <select name="status" class="form-control">
                                                                @if($slider->status == 1)
                                                                    <option value="1" class="form-control">Active</option>
                                                                    <option value="0" class="form-control">Inactive</option>
                                                                @else
                                                                    <option value="0" class="form-control">Inactive</option>
                                                                    <option value="1" class="form-control">Active</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary btn-lg">Update Save</button>
                                                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addSlider" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Slider</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('save.slider') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label">Image <span class="text-danger">*</span></label>
                            <input type="file" name="image" value="{{ old('image') }}" class="form-control" placeholder=" Image">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Text<span class="text-danger"></span></label>
                            <textarea id="editor" name="text" class="form-control">{{ old('text') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Button Name<span class="text-danger"></span></label>
                            <input type="text" name="button" value="{{ old('button') }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Button Link<span class="text-danger"></span></label>
                            <input type="text" name="link" value="{{ old('link') }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <select name="status" class="form-control">
                                <option value="1" class="form-control">Active</option>
                                <option value="0" class="form-control">Inactive</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg">Save</button>
                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection
