@extends('frontend.master')

@section('title')
    Home || CliniParts
@endsection

@push('customcss')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
@endpush

@section('content')

    <!-- Hero Section Start -->
    <div class="hero-section section mb-30">
        <div class="container">
            <div class="row">
                <div class="col">

                    <!-- Hero Slider Start -->
                    <div class="hero-slider hero-slider-one">

                        <!-- Hero Item Start -->
                        @foreach($sliders as $slider)
                        <div class="hero-item">
                            <div class="row align-items-center justify-content-between">

                                <!-- Hero Content -->
                                <div class="hero-content col">

                                    <p>{!! $slider['text'] !!}</p>
                                    @if($slider->link)
                                        <a href="{{ $slider['link'] }}">{{ $slider['button'] }}</a>
                                    @else
                                        <a href="">{{ $slider['button'] }}</a>
                                    @endif

                                </div>

                                <!-- Hero Image -->
                                <div class="hero-image col">
                                    <img src="{{ asset('assets/backend/images/HomePage/'.$slider->image) }}" alt="Hero Image">
                                </div>

                            </div>
                        </div><!-- Hero Item End -->
                        @endforeach

{{--                        <!-- Hero Item Start -->--}}
{{--                        <div class="hero-item">--}}
{{--                            <div class="row align-items-center justify-content-between">--}}

{{--                                <!-- Hero Content -->--}}
{{--                                <div class="hero-content col">--}}

{{--                                    <h2>HURRY UP!</h2>--}}
{{--                                    <h1><span>GL G6</span></h1>--}}
{{--                                    <h1>IT’S <span class="big">35%</span> OFF</h1>--}}
{{--                                    <a href="#">get it now</a>--}}

{{--                                </div>--}}

{{--                                <!-- Hero Image -->--}}
{{--                                <div class="hero-image col">--}}
{{--                                    <img src="assets/images/hero/hero-2.png" alt="Hero Image">--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div><!-- Hero Item End -->--}}

{{--                        <!-- Hero Item Start -->--}}
{{--                        <div class="hero-item">--}}
{{--                            <div class="row align-items-center justify-content-between">--}}

{{--                                <!-- Hero Content -->--}}
{{--                                <div class="hero-content col">--}}

{{--                                    <h2>HURRY UP!</h2>--}}
{{--                                    <h1><span>MSVII Case</span></h1>--}}
{{--                                    <h1>IT’S <span class="big">15%</span> OFF</h1>--}}
{{--                                    <a href="#">get it now</a>--}}

{{--                                </div>--}}

{{--                                <!-- Hero Image -->--}}
{{--                                <div class="hero-image col">--}}
{{--                                    <img src="assets/images/hero/hero-3.png" alt="Hero Image">--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div><!-- Hero Item End -->--}}

                    </div><!-- Hero Slider End -->

                </div>
            </div>
        </div>
    </div>
    <!-- Hero Section End -->


    <!-- Banner Section Start -->
    <div class="banner-section section mb-60">
        <div class="container">
            <div class="row row-10">

                <!-- Banner -->
                <div class="col-md-8 col-12 mb-30">
                    <div class="banner">
                        <a href="">
                            <img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image3) }}" alt="Banner" style="height: 300px">
                        </a>
                    </div>
                </div>

                <!-- Banner -->
                <div class="col-md-4 col-12 mb-30">
                    <div class="banner">
                        <a href="">
                            <img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image4) }}" alt="Banner" style="height: 300px">
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Banner Section End -->

    <section class="mb-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-4" style="box-shadow: 2px 2px 5px #284e64; background: #ffffff">
                    <div class="col-lg-3 float-left">
                        <h2>{{ $homeText['name2'] }}</h2>
                    </div>
                    <div class="col-lg-9 float-left">

                        <form class="">
                            <select class="product-advance-search" searchable="Search Manufacturer">
                                <option value="" disabled selected>Manufacturer</option>
                                <option value="1">3M Healthcare</option>
                                <option value="1">3M Healthcare (formerly Arizant Healthcare, Inc.)</option>
                                <option value="2">A-M Systems</option>
                                <option value="3">A-dec</option>
                                <option value="4">AB Sciex LLC</option>
                            </select>

                            <select class="product-advance-search" searchable="Search Model">
                                <option value="" disabled selected>Model</option>
                                <option value="1">5500</option>
                                <option value="2">API 3200</option>
                                <option value="3">200 EXTERNAL FEEDING PUMP</option>
                                <option value="4">BLOOD PUMP</option>
                                <option value="5">HEPA 10</option>
                            </select>

                            <button class="btn btn-hover btn-blue btn-sm">Find</button>
                        </form>
                    </div>
                </div>
                <!--            <div class="col-md-1"></div>-->
            </div>
        </div>
    </section>


    <!-- Feature Product Section Start -->
    <div class="product-section section mb-70">
        <div class="container">
            <div class="row">

                <!-- Section Title Start -->
                <div class="col-12 mb-40">
                    <div class="section-title-one" data-title="FEATURED ITEMS"><h1 class="text-yellow">{{ $homeText['name3'] }}</h1></div>
                </div><!-- Section Title End -->

                <!-- Product Tab Filter Start -->
                <div class="col-12 mb-30">
                    <div class="product-tab-filter">

                        <!-- Tab Filter Toggle -->
                        <button class="product-tab-filter-toggle">showing: <span></span><i class="icofont icofont-simple-down"></i></button>

                        <!-- Product Tab List -->
                        <ul class="nav product-tab-list">
                            <li><a class="active" data-toggle="tab" href="#tab-one">all</a></li>
                            <li><a data-toggle="tab" href="#tab-two">Batteries</a></li>
                            <li><a data-toggle="tab" href="#tab-one">Biomedical</a></li>
                            <li><a data-toggle="tab" href="#tab-two">Cables & Sensors</a></li>
                            <li><a data-toggle="tab" href="#tab-one">Laboratory</a></li>
                            <li><a data-toggle="tab" href="#tab-two">Minor Equipment</a></li>
                        </ul>

                    </div>
                </div><!-- Product Tab Filter End -->

                <!-- Product Tab Content Start -->
                <div class="col-12">
                    <div class="tab-content">

                        <!-- Tab Pane Start -->
                        <div class="tab-pane fade show active" id="tab-one">

                            <!-- Product Slider Wrap Start -->
                            <div class="product-slider-wrap product-slider-arrow-one">
                                <!-- Product Slider Start -->
                                <div class="product-slider product-slider-4">

                                    @foreach($featured_products as $featured)
                                    <div class="col pb-20 pt-10">
                                        <!-- Product Start -->
                                        <div class="ee-product">
                                            <!-- Image -->
                                            <div class="image">
                                                <span class="label sale">{{ $featured['top_title'] }}</span>
                                                <a href="{{ route('product.details',['id'=>$featured->id]) }}" class="img">
                                                    <img src="{{ asset('assets/backend/images/Product/'.$featured['image1']) }}" alt="Product Image" style="max-height: 250px">
                                                </a>
                                                <div class="wishlist-compare">
                                                    <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>
                                                    <a href="#" data-tooltip="Wishlist"><i class="ti-heart"></i></a>
                                                </div>
{{--                                                <button ><i class="ti-shopping-cart add-to-cart2" data-id="{{ $featured->id }}"></i><span>ADD TO CART</span></button>--}}
                                                <div class="text-center bg-primary">
                                                    <a href="{{ route('add.to.cart',['id' => $featured->id]) }}" class="btn btn-primary"><i class="ti-shopping-cart"></i><span>ADD TO CART</span></a>
                                                </div>
                                            </div>
                                            <!-- Content -->
                                            <div class="content">
                                                <!-- Category & Title -->
                                                <div class="category-title">

                                                    @if($featured->sub_subCategory_id)
                                                        <a href="#" class="cat">{{ $featured->subSubCategoryName->subCategoryName['name'] }}</a>
                                                    @else
                                                        <a href="#" class="cat">{{ $featured->subSubCategoryName['name'] }}</a>
                                                    @endif
                                                    <h5 class="title"><a href="">{{ $featured['name'] }}</a></h5>
                                                </div>

                                                <!-- Price & Ratting -->
                                                <div class="price-ratting">

                                                    @if($featured->old_price)
                                                        <h5 class="price"><span class="old">${{ $featured['old_price'] }}</span>${{ $featured['price'] }}</h5>
                                                    @else
                                                        <h5 class="price">${{ $featured['price'] }}</h5>
                                                    @endif
                                                    <div class="ratting">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-half-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </div>

                                                </div>

                                            </div>

                                        </div><!-- Product End -->
                                    </div>
                                    @endforeach

                                </div><!-- Product Slider End -->
                            </div><!-- Product Slider Wrap End -->

                        </div><!-- Tab Pane End -->
{{--                        <script type="text/javascript">--}}
{{--                            $(document).ready(function () {--}}
{{--                                $('.add-to-cart2').on('click',function () {--}}
{{--                                    var id = $(this).data('id');--}}
{{--                                    alert('ok')--}}
{{--                                    if(id){--}}
{{--                                        $.ajax({--}}
{{--                                            url: '{{ url('add-to-cart') }}/'+id,--}}
{{--                                            type: "GET",--}}
{{--                                            dataType: "json",--}}
{{--                                            success: function (data) {--}}

{{--                                                window.location.reload();--}}
{{--                                            }--}}
{{--                                        });--}}
{{--                                    }--}}
{{--                                })--}}
{{--                            })--}}
{{--                        </script>--}}

                        <!-- Tab Pane Start -->
                        <div class="tab-pane fade" id="tab-two">

                            <!-- Product Slider Wrap Start -->
                            <div class="product-slider-wrap product-slider-arrow-one">
                                <!-- Product Slider Start -->
                                <div class="product-slider product-slider-4">

                                    <div class="col pb-20 pt-10">
                                        <!-- Product Start -->
                                        <div class="ee-product">

                                            <!-- Image -->
                                            <div class="image">

                                                <span class="label sale">sale</span>

                                                <a href="shop.html" class="img"><img src="assets/images/product/product-6.png" alt="Product Image"></a>

                                                <div class="wishlist-compare">
                                                    <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>
                                                    <a href="#" data-tooltip="Wishlist"><i class="ti-heart"></i></a>
                                                </div>

                                                <a href="#" class="add-to-cart"><i class="ti-shopping-cart"></i><span>ADD TO CART</span></a>

                                            </div>

                                            <!-- Content -->
                                            <div class="content">

                                                <!-- Category & Title -->
                                                <div class="category-title">

                                                    <a href="#" class="cat">MINOR EQUIPMENT</a>
                                                    <h5 class="title"><a href="shop.html">3500 SYRINGE INFUSION PUMP by Smiths Medical</a></h5>

                                                </div>

                                                <!-- Price & Ratting -->
                                                <div class="price-ratting">

                                                    <h5 class="price"><span class="old">$265</span>$199.00</h5>
                                                    <div class="ratting">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-half-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </div>

                                                </div>

                                            </div>

                                        </div><!-- Product End -->
                                    </div>

                                    <div class="col pb-20 pt-10">
                                        <!-- Product Start -->
                                        <div class="ee-product">

                                            <!-- Image -->
                                            <div class="image">

                                                <a href="shop.html" class="img"><img src="assets/images/product/product-7.png" alt="Product Image"></a>

                                                <div class="wishlist-compare">
                                                    <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>
                                                    <a href="#" data-tooltip="Wishlist"><i class="ti-heart"></i></a>
                                                </div>

                                                <a href="#" class="add-to-cart"><i class="ti-shopping-cart"></i><span>ADD TO CART</span></a>

                                            </div>

                                            <!-- Content -->
                                            <div class="content">

                                                <!-- Category & Title -->
                                                <div class="category-title">

                                                    <a href="#" class="cat">MINOR EQUIPMENT</a>
                                                    <h5 class="title"><a href="shop.html">4000 SYRINGE INFUSION PUMP by Smiths Medical</a></h5>

                                                </div>

                                                <!-- Price & Ratting -->
                                                <div class="price-ratting">

                                                    <h5 class="price">$580.00</h5>
                                                    <div class="ratting">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </div>

                                                </div>

                                            </div>

                                        </div><!-- Product End -->
                                    </div>

                                    <div class="col pb-20 pt-10">
                                        <!-- Product Start -->
                                        <div class="ee-product">

                                            <!-- Image -->
                                            <div class="image">

                                                <span class="label new">new</span>

                                                <a href="shop.html" class="img"><img src="assets/images/product/product-8.png" alt="Product Image"></a>

                                                <div class="wishlist-compare">
                                                    <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>
                                                    <a href="#" data-tooltip="Wishlist"><i class="ti-heart"></i></a>
                                                </div>

                                                <a href="#" class="add-to-cart"><i class="ti-shopping-cart"></i><span>ADD TO CART</span></a>

                                            </div>

                                            <!-- Content -->
                                            <div class="content">

                                                <!-- Category & Title -->
                                                <div class="category-title">

                                                    <a href="#" class="cat">LABORATORY</a>
                                                    <h5 class="title"><a href="shop.html">2001 SYRINGE INFUSION PUMP by Smiths Medical</a></h5>

                                                </div>

                                                <!-- Price & Ratting -->
                                                <div class="price-ratting">

                                                    <h5 class="price">$320.00</h5>
                                                    <div class="ratting">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>

                                                </div>

                                            </div>

                                        </div><!-- Product End -->
                                    </div>

                                    <div class="col pb-20 pt-10">
                                        <!-- Product Start -->
                                        <div class="ee-product">

                                            <!-- Image -->
                                            <div class="image">

                                                <span class="label sale">sale</span>

                                                <a href="shop.html" class="img"><img src="assets/images/product/product-9.png" alt="Product Image"></a>

                                                <div class="wishlist-compare">
                                                    <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>
                                                    <a href="#" data-tooltip="Wishlist"><i class="ti-heart"></i></a>
                                                </div>

                                                <a href="#" class="add-to-cart"><i class="ti-shopping-cart"></i><span>ADD TO CART</span></a>

                                            </div>

                                            <!-- Content -->
                                            <div class="content">

                                                <!-- Category & Title -->
                                                <div class="category-title">

                                                    <a href="#" class="cat">BIOMEDICAL</a>
                                                    <h5 class="title"><a href="shop.html">INFUS O.R. INFUSION PUMP</a></h5>

                                                </div>

                                                <!-- Price & Ratting -->
                                                <div class="price-ratting">

                                                    <h5 class="price"><span class="old">$365</span>$295.00</h5>
                                                    <div class="ratting">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-half-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </div>

                                                </div>

                                            </div>

                                        </div><!-- Product End -->
                                    </div>

                                    <div class="col pb-20 pt-10">
                                        <!-- Product Start -->
                                        <div class="ee-product">

                                            <!-- Image -->
                                            <div class="image">

                                                <a href="shop.html" class="img"><img src="assets/images/product/product-10.png" alt="Product Image"></a>

                                                <div class="wishlist-compare">
                                                    <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>
                                                    <a href="#" data-tooltip="Wishlist"><i class="ti-heart"></i></a>
                                                </div>

                                                <a href="#" class="add-to-cart"><i class="ti-shopping-cart"></i><span>ADD TO CART</span></a>

                                            </div>

                                            <!-- Content -->
                                            <div class="content">

                                                <!-- Category & Title -->
                                                <div class="category-title">

                                                    <a href="#" class="cat">BIOMEDICAL</a>
                                                    <h5 class="title"><a href="shop.html">LI-ION 9 CELL BATTERY PACK by Philips Healthcare (Medical Supplies)</a></h5>

                                                </div>

                                                <!-- Price & Ratting -->
                                                <div class="price-ratting">

                                                    <h5 class="price">$189.00</h5>
                                                    <div class="ratting">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>

                                                </div>

                                            </div>

                                        </div><!-- Product End -->
                                    </div>

                                </div><!-- Product Slider End -->
                            </div><!-- Product Slider Wrap End -->

                        </div><!-- Tab Pane End -->

                    </div>
                </div><!-- Product Tab Content End -->

            </div>
        </div>
    </div>
    <!-- Feature Product Section End -->

    <!-- Best Sell Product Section Start -->
    <div class="product-section section mb-60">
        <div class="container">
            <div class="row">

                <!-- Section Title Start -->
                <div class="col-12 mb-40">
                    <div class="section-title-one" data-title="BEST SELLER"><h1 class="text-yellow">{{ $homeText['name4'] }}</h1></div>
                </div><!-- Section Title End -->

                <div class="col-12">
                    <div class="row">

                        @foreach($products as $product)
                        <div class="col-xl-3 col-lg-4 col-md-6 col-12 pb-30 pt-10">
                            <!-- Product Start -->
                            <div class="ee-product">

                                <!-- Image -->
                                <div class="image">
                                    <span class="label sale">{{ $product['top_title'] }}</span>

                                    <a href="{{ route('product.details',['id'=>$product->id]) }}" class="img">
                                        <img src="{{ asset('assets/backend/images/Product/'.$product['image1']) }}" alt="Product Image" style="max-height: 250px">
                                    </a>

                                    <div class="wishlist-compare">
                                        <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>
                                        <a href="#" data-tooltip="Wishlist"><i class="ti-heart"></i></a>
                                    </div>

{{--                                    <a href="#" class="add-to-cart"><i class="ti-shopping-cart"></i><span>ADD TO CART</span></a>--}}
                                    <div class="text-center bg-primary">
                                        <a href="{{ route('add.to.cart',['id' => $product->id]) }}" class="btn btn-primary"><i class="ti-shopping-cart"></i><span>ADD TO CART</span></a>
                                    </div>
                                </div>

                                <!-- Content -->
                                <div class="content">

                                    <!-- Category & Title -->
                                    <div class="category-title">

                                        @if($product->sub_subCategory_id)
                                            <a href="#" class="cat">{{ $product->subSubCategoryName->subCategoryName['name'] }}</a>
                                        @else
                                            <a href="#" class="cat">{{ $product->subSubCategoryName['name'] }}</a>
                                        @endif
                                        <h5 class="title"><a href="">{{ $product['name'] }}</a></h5>

                                    </div>

                                    <!-- Price & Ratting -->
                                    <div class="price-ratting">

                                        @if($product->old_price)
                                            <h5 class="price"><span class="old">${{ $product['old_price'] }}</span>${{ $product['price'] }}</h5>
                                        @else
                                            <h5 class="price">${{ $product['price'] }}</h5>
                                        @endif
                                        <div class="ratting">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>

                                    </div>

                                </div>

                            </div><!-- Product End -->
                        </div>
                        @endforeach

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Best Sell Product Section End -->

    <!-- Banner Section Start -->
    <div class="banner-section section mb-90">
        <div class="container">
            <div class="row">

                <!-- Banner -->
                <div class="col-12">
                    <div class="banner">
                        <a href="#">
                            <img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image5) }}" alt="Banner" style="max-height: 300px">
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Banner Section End -->

    <!-- Feature Section Start -->
    <div class="feature-section section mb-60">
        <div class="container">
            <div class="row">

                @foreach($homeSections as $section)
                <div class="col-lg-4 col-md-6 col-12 mb-30">
                    <!-- Feature Start -->
                    <div class="feature feature-shipping">
                        <div class="feature-wrap">
                            <div class="icon">
                                <img src="{{ asset('assets/backend/images/HomePage/'.$section['image']) }}" alt="Feature">
                            </div>
                            <h4>{{ $section['title'] }}</h4>
                            <p>{{ $section['des'] }}</p>
                        </div>
                    </div><!-- Feature End -->
                </div>
                @endforeach

            </div>
        </div>
    </div>
    <!-- Feature Section End -->

    <!-- Best Deals Product Section End -->

    <!-- New Arrival Product Section Start -->
    <div class="product-section section mb-60">
        <div class="container">
            <div class="row">

                <!-- Section Title Start -->
                <div class="col-12 mb-40">
                    <div class="section-title-one" data-title="NEW ARRIVAL"><h1 class="text-yellow">{{ $homeText['name5'] }}</h1></div>
                </div><!-- Section Title End -->

                <div class="col-12">
                    <div class="row">

                        @foreach($products as $product)
                        <div class="col-xl-3 col-lg-4 col-md-6 col-12 pb-30 pt-10">
                            <!-- Product Start -->
                            <div class="ee-product">

                                <!-- Image -->
                                <div class="image">

                                    <span class="label sale">{{ $product['top_title'] }}</span>

                                    <a href="{{ route('product.details',['id'=>$product->id]) }}" class="img">
                                        <img src="{{ asset('assets/backend/images/Product/'.$product['image1']) }}" alt="Product Image" style="max-height: 250px">
                                    </a>

                                    <div class="wishlist-compare">
                                        <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>
                                        <a href="#" data-tooltip="Wishlist"><i class="ti-heart"></i></a>
                                    </div>


                                    <div class="text-center bg-primary">

                                        <a href="javascript:void(0);" OnClick="addToCart({{$product->id}});"  class="btn btn-primary"><i class="ti-shopping-cart"></i><span>ADD TO CART</span></a>
                                    </div>
                                </div>

                                <!-- Content -->
                                <div class="content">

                                    <!-- Category & Title -->
                                    <div class="category-title">

                                        @if($product->sub_subCategory_id)
                                            <a href="#" class="cat">{{ $product->subSubCategoryName->subCategoryName['name'] }}</a>
                                        @else
                                            <a href="#" class="cat">{{ $product->subSubCategoryName['name'] }}</a>
                                        @endif
                                        <h5 class="title"><a href="">{{ $product['name'] }}</a></h5>

                                    </div>

                                    <!-- Price & Ratting -->
                                    <div class="price-ratting">

                                        @if($product->old_price)
                                            <h5 class="price"><span class="old">${{ $product['old_price'] }}</span>${{ $product['price'] }}</h5>
                                        @else
                                            <h5 class="price">${{ $product['price'] }}</h5>
                                        @endif
                                        <div class="ratting">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>

                                    </div>

                                </div>

                            </div><!-- Product End -->
                        </div>
                        @endforeach

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- New Arrival Product Section End -->

    <!-- Banner Section Start -->
    <div class="banner-section section mb-60">
        <div class="container">
            <div class="row">

                <!-- Banner -->
                <div class="col-md-4 col-12 mb-30">
                    <div class="banner">
                        <a href="#">
                            <img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image6) }}" alt="Banner" style="max-height: 300px">
                        </a>
                    </div>
                </div>

                <!-- Banner -->
                <div class="col-md-4 col-12 mb-30">
                    <div class="banner">
                        <a href="#">
                            <img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image7) }}" alt="Banner" style="max-height: 300px">
                        </a>
                    </div>
                </div>

                <!-- Banner -->
                <div class="col-md-4 col-12 mb-30">
                    <div class="banner">
                        <a href="#">
                            <img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image8) }}" alt="Banner" style="max-height: 300px">
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Banner Section End -->

    <!-- Brands Section Start -->
    <div class="brands-section section mb-90">
        <div class="container">
            <div class="row">

                <!-- Brand Slider Start -->
                <div class="brand-slider col">
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image9) }}" alt="Brands"></div>
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image10) }}" alt="Brands"></div>
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image11) }}" alt="Brands"></div>
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image12) }}" alt="Brands"></div>
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image13) }}" alt="Brands"></div>
                </div><!-- Brand Slider End -->

            </div>
        </div>
    </div>
    <!-- Brands Section End -->


    <!-- Popup Subscribe Section Start -->
    <div class="popup-subscribe-section section bg-gray pt-55 pb-55" data-modal="popup-modal">

        <!-- Popup Subscribe Wrap Start -->
        <div class="popup-subscribe-wrap">

            <button class="close-popup">X</button>

            <!-- Popup Subscribe Banner -->
            <div class="popup-subscribe-banner banner">
                <a href="#"><img src="{{ asset('assets/backend/images/HomePage/'.$subscribe->image) }}" alt="Banner"></a>
            </div>

            <!-- Popup Subscribe Form Wrap Start -->
            <div class="popup-subscribe-form-wrap">
                <h2 class="text-center text-success">{{ Session::get('message') }}</h2>
                <h1 class="text-blue">{{ $subscribe['title1'] }}</h1>
                <h4 class="text-blue">{{ $subscribe['title2'] }}</h4>

                <!-- Newsletter Form -->
                <form action="{{ route('send.subscribe') }}" method="POST" class="popup-subscribe-form validate" novalidate>
                    @csrf
                    <div id="mc_embed_signup_scroll">
                        <label for="popup_subscribe" class="d-none text-blue">Subscribe to our mailing list</label>
                        <input type="email" value="" name="email" class="email" id="popup_subscribe" placeholder="{{ $subscribe['place'] }}" required>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="" tabindex="-1" value=""></div>
                        <button type="submit" name="subscribe" id="" class="button">{{ $subscribe['button'] }}</button>
                    </div>
                </form>

                <p>{{ $subscribe['des'] }}</p>

            </div>
            <!-- Popup Subscribe Form Wrap End -->

        </div>
        <!-- Popup Subscribe Wrap End -->

    </div>
    <!-- Popup Subscribe Section End -->
@endsection

@push('jscripts')

@endpush
