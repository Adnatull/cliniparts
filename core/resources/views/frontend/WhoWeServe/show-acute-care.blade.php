@extends('frontend.master')

@section('title')
    Acute Care :: Cliniparts
@endsection

@section('content')
    <!-- Page Banner Section Start -->
    <div class="page-banner-section section">
        <div class="page-banner-wrap row row-0 d-flex align-items-center ">

            <!-- Page Banner -->
            <div class="col-lg-4 col-12 order-lg-2 d-flex align-items-center justify-content-center">
                <div class="page-banner">
                    <h1>{{ $acute['title1'] }}</h1>
                    <p>similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita</p>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="{{ route('/') }}">HOME</a></li>
                            <li><a href="">{{ $acute['title2'] }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-1">
                <div class="banner">
                    <a href="#">
                        <img src="{{ asset('assets/backend/images/WhoWeServe/'.$acute['image1']) }}" alt="Banner" style="max-height: 220px">
                    </a>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-3">
                <div class="banner">
                    <a href="#">
                        <img src="{{ asset('assets/backend/images/WhoWeServe/'.$acute['image2']) }}" alt="Banner" style="max-height: 220px">
                    </a>
                </div>
            </div>

        </div>
    </div><!-- Page Banner Section End -->

    <!-- About Section Start -->
    <div class="about-section section mt-90 mb-30">
        <div class="container">

            <div class="row">

                <!-- Feature Content -->
                <div class="feature-content col-lg-6 col-12 order-2 order-lg-1 mb-30 text-blue">
                    <p>{!! $acute['m_title'] !!}</p>
                    <p>{{ $acute['m_des'] }}</p>
                </div>

                <!-- Feature Image -->
                <div class="col-lg-5 col-12 order-1 order-lg-2 ml-auto mb-30">
                    <div class="feature-image"><img src="{{ asset('assets/backend/images/WhoWeServe/'.$acute['image3']) }}" alt=""></div>
                </div>

            </div>

            <div class="row mb-30">

                <!-- About Feature -->
                <div class="about-feature col-lg-9 col-12">
                    <div class="row">

                        <div class="col-lg-4 col-md-6 col-12 mb-30 text-blue">
                            <h4 class="text-blue">{{ $acute['s_title1'] }}</h4>
                            <p>{{ $acute['s_des1'] }} </p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 mb-30 text-blue">
                            <h4 class="text-blue">{{ $acute['s_title2'] }}</h4>
                            <p>{{ $acute['s_des2'] }} </p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 mb-30 d-none d-lg-block"></div>

                        <div class="col-lg-4 col-md-6 col-12 mb-30 text-blue">
                            <h4 class="text-blue">{{ $acute['s_title3'] }}</h4>
                            <p>{{ $acute['s_des3'] }} </p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 mb-30 text-blue">
                            <h4 class="text-blue">{{ $acute['s_title4'] }}</h4>
                            <p>{{ $acute['s_des4'] }} </p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 mb-30 text-blue">
                            <h4 class="text-blue">{{ $acute['s_title5'] }}</h4>
                            <p>{{ $acute['s_des5'] }} </p>
                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div><!-- About Section End -->

    <!-- Banner Section Start -->
    <div class="banner-section section mb-90">
        <div class="container">
            <div class="row">

                <div class="col">
                    <div class="banner"><a href="#"><img src="{{ asset('assets/backend/images/WhoWeServe/'.$acute['image4']) }}" alt="Banner"></a></div>
                </div>

            </div>
        </div>
    </div><!-- Banner Section End -->
@endsection
