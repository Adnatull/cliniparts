@extends('frontend.master')

@section('title')
    Contact Us :: Cliniparts
@endsection

@section('content')
    <!-- Page Banner Section Start -->
    <div class="page-banner-section section">
        <div class="page-banner-wrap row row-0 d-flex align-items-center ">

            <!-- Page Banner -->
            <div class="col-lg-4 col-12 order-lg-2 d-flex align-items-center justify-content-center">
                <div class="page-banner">
                    <h1>{{ $contact['title1'] }}</h1>
                    <p>similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita</p>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="{{ route('/') }}">HOME</a></li>
                            <li><a href="">{{ $contact['title1'] }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-1">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/About/'.$contact['image1']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-3">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/About/'.$contact['image2']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

        </div>
    </div><!-- Page Banner Section End -->

    <!-- Contact Section Start -->
    <div class="contact-section section mt-90 mb-40">
        <div class="container">
            <div class="row">

                <!-- Contact Page Title -->
                <div class="contact-page-title col mb-40">
                    <h1 class="text-blue">{{ $contact['m_title'] }}</h1>
                </div>
            </div>
            <div class="row">

                <!-- Contact Tab List -->
                <div class="col-lg-4 col-12 mb-50">
                    <ul class="contact-tab-list nav">
                        <li><a class="active" data-toggle="tab" href="#contact-address">{{ $contact['title3'] }}</a></li>
                        <li><a data-toggle="tab" href="#contact-form-tab">{{ $contact['title4'] }}</a></li>
                        <li><a data-toggle="tab" href="#store-location">{{ $contact['title5'] }}</a></li>
                    </ul>
                </div>

                <!-- Contact Tab Content -->
                <div class="col-lg-8 col-12">
                    <div class="tab-content">

                        <!-- Contact Address Tab -->
                        <div class="tab-pane fade show active row d-flex" id="contact-address">

                            <div class="col-lg-4 col-md-6 col-12 mb-50">
                                <div class="contact-information">
                                    <h4 class="text-blue">{{ $contact['address'] }}</h4>
                                    <p class="text-blue">{{ $contact['address_des'] }}</p>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-12 mb-50">
                                <div class="contact-information">
                                    <h4 class="text-blue">{{ $contact['phone'] }}</h4>
                                    <p class="text-blue">{{ $contact['phone_des'] }}</p>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-12 mb-50">
                                <div class="contact-information">
                                    <h4 class="text-blue">{{ $contact['web'] }}</h4>
                                    <p class="text-blue">{{ $contact['web_des'] }}</p>
                                </div>
                            </div>

                        </div>

                        <!-- Contact Form Tab -->
                        <div class="tab-pane fade row d-flex" id="contact-form-tab">
                            <div class="col">

                                <form id="contact-form" action="" method="post" class="contact-form mb-50">
                                    <div class="row">
                                        <div class="col-md-6 col-12 mb-25">
                                            <label for="first_name" class="text-blue">First Name*</label>
                                            <input id="first_name" type="text" name="first_name">
                                        </div>
                                        <div class="col-md-6 col-12 mb-25">
                                            <label for="last_name" class="text-blue">Last Name*</label>
                                            <input id="last_name" type="text" name="last_name">
                                        </div>
                                        <div class="col-md-6 col-12 mb-25">
                                            <label for="email_address" class="text-blue">Email*</label>
                                            <input id="email_address" type="email" name="email_address">
                                        </div>
                                        <div class="col-md-6 col-12 mb-25">
                                            <label for="phone_number" class="text-blue">Phone</label>
                                            <input id="phone_number" type="text" name="phone_number">
                                        </div>
                                        <div class="col-12 mb-25">
                                            <label for="message" class="text-blue">Message*</label>
                                            <textarea id="message" name="message"></textarea>
                                        </div>
                                        <div class="col-12">
                                            <input type="submit" value="SEND NOW">
                                        </div>
                                    </div>
                                </form>
                                <p class="form-messege"></p>

                            </div>
                        </div>

                        <!-- Contact Stores Tab -->
                        <div class="tab-pane fade row d-flex" id="store-location">

                            @foreach($stores as $store)
                            <div class="col-md-6 col-12 mb-50">
                                <div class="single-store text-blue">
                                    <h3 class="text-blue">{{ $store['name'] }}</h3>
                                    <p>{{ $store['des'] }}</p>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Contact Section End -->

    <!-- Brands Section Start -->
    <div class="brands-section section mb-90">
        <div class="container">
            <div class="row">

                <!-- Brand Slider Start -->
                <div class="brand-slider col">
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image9) }}" alt="Brands"></div>
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image10) }}" alt="Brands"></div>
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image11) }}" alt="Brands"></div>
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image12) }}" alt="Brands"></div>
                    <div class="brand-item col"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image13) }}" alt="Brands"></div>
                </div><!-- Brand Slider End -->

            </div>
        </div>
    </div><!-- Brands Section End -->
@endsection
