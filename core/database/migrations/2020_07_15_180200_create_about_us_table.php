<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('image1')->nullable();
            $table->text('image2')->nullable();
            $table->string('title1')->nullable();
            $table->string('title2')->nullable();
            $table->text('image3')->nullable();
            $table->string('wc_title1')->nullable();
            $table->string('wc_title2')->nullable();
            $table->longText('wc_des')->nullable();
            $table->string('we_title')->nullable();
            $table->longText('we_des')->nullable();
            $table->string('win_title')->nullable();
            $table->longText('win_des')->nullable();
            $table->text('image4')->nullable();
            $table->text('image5')->nullable();
            $table->text('image6')->nullable();
            $table->string('vision_title')->nullable();
            $table->longText('vision_des')->nullable();
            $table->string('mission_title')->nullable();
            $table->longText('mission_des')->nullable();
            $table->string('goal_title')->nullable();
            $table->longText('goal_des')->nullable();
            $table->text('you_title')->nullable();
            $table->longText('you_des')->nullable();
            $table->text('image7')->nullable();
            $table->string('fast_title')->nullable();
            $table->longText('fast_des')->nullable();
            $table->string('quality_title')->nullable();
            $table->longText('quality_des')->nullable();
            $table->string('secure_title')->nullable();
            $table->longText('secure_des')->nullable();
            $table->string('money_title')->nullable();
            $table->longText('money_des')->nullable();
            $table->string('easy_title')->nullable();
            $table->longText('easy_des')->nullable();
            $table->string('free_title')->nullable();
            $table->longText('free_des')->nullable();
            $table->string('support_title')->nullable();
            $table->longText('support_des')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us');
    }
}
