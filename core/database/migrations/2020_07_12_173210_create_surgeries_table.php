<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurgeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surgeries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('image1')->nullable();
            $table->text('image2')->nullable();
            $table->string('title1')->nullable();
            $table->string('title2')->nullable();
            $table->longText('m_title')->nullable();
            $table->longText('m_des')->nullable();
            $table->string('image3')->nullable();
            $table->string('s_title1')->nullable();
            $table->text('s_des1')->nullable();
            $table->string('s_title2')->nullable();
            $table->text('s_des2')->nullable();
            $table->string('s_title3')->nullable();
            $table->text('s_des3')->nullable();
            $table->string('s_title4')->nullable();
            $table->text('s_des4')->nullable();
            $table->string('s_title5')->nullable();
            $table->text('s_des5')->nullable();
            $table->text('image4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgeries');
    }
}
