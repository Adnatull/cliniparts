<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubSubCategory extends Model
{
    protected $guarded = [];

    public function subCategoryName(){
        return $this->belongsTo(SubCategory::class, 'subcategory_id', 'id');
    }

    public static function addSubSubCategoryData($request){
        SubSubCategory::create([
            'subcategory_id' => $request->subcategory_id,
            'name' => $request->name,
            'status' => $request->status,
        ]);
    }
    public static function updateSubSubCategoryData($request){
        $subSubCategory = SubSubCategory::find($request->id);
        $subSubCategory->subcategory_id = $request->subcategory_id;
        $subSubCategory->name = $request->name;
        $subSubCategory->status = $request->status;
        $subSubCategory->save();
    }
    public static function deleteSubSubCategoryData($request){
        $subSubCategory = SubSubCategory::find($request->id);
        $subSubCategory->delete();
    }
}
