<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    protected $guarded = [];

    public static function addNewsCategoryData($request){
        NewsCategory::create([
            'name' => $request->name,
            'status' => $request->status,
        ]);
    }
    public static function updateNewsCategoryData($request){
        $category = NewsCategory::find($request->id);
        $category->name = $request->name;
        $category->status = $request->status;
        $category->save();
    }
    public static function deleteNewsCategoryData($request){
        $category = NewsCategory::find($request->id);
        $category->delete();
    }
}
