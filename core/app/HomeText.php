<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeText extends Model
{
    public static function addHomeTextData($request){
        $homeText = HomeText::first();
        $homeText->name1 = $request->name1;
        $homeText->name2 = $request->name2;
        $homeText->name3 = $request->name3;
        $homeText->name4 = $request->name4;
        $homeText->name5 = $request->name5;
        $homeText->name6 = $request->name6;
        $homeText->name7 = $request->name7;
        $homeText->name8 = $request->name8;
        $homeText->name9 = $request->name9;
        $homeText->contact = $request->contact;
        $homeText->save();
    }
}
