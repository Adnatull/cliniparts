<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Contact extends Model
{
    public static function updateContactData($request){
        $contact = Contact::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/About/'.$contact->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/About/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $contact->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/About/'.$contact->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/About/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $contact->image2 = $imageName;
        }
        $contact->title1 = $request->title1;
        $contact->title2 = $request->title2;
        $contact->m_title = $request->m_title;
        $contact->title3 = $request->title3;
        $contact->title4 = $request->title4;
        $contact->title5 = $request->title5;
        $contact->address = $request->address;
        $contact->address_des = $request->address_des;
        $contact->phone = $request->phone;
        $contact->phone_des = $request->phone_des;
        $contact->web = $request->web;
        $contact->web_des = $request->web_des;
        $contact->save();
    }
}
