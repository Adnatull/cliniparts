<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class HomeHealth extends Model
{
    public static function addHomeHealthData($request){
        $home = HomeHealth::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$home->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$home->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$home->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$home->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image4 = $imageName;
        }
        $home->title1 = $request->title1;
        $home->title2 = $request->title2;
        $home->m_title = $request->m_title;
        $home->m_des = $request->m_des;
        $home->s_title1 = $request->s_title1;
        $home->s_des1 = $request->s_des1;
        $home->s_title2 = $request->s_title2;
        $home->s_des2 = $request->s_des2;
        $home->s_title3 = $request->s_title3;
        $home->s_des3 = $request->s_des3;
        $home->s_title4 = $request->s_title4;
        $home->s_des4 = $request->s_des4;
        $home->s_title5 = $request->s_title5;
        $home->s_des5 = $request->s_des5;
        $home->save();
    }
}
