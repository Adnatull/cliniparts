<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Integrated extends Model
{
    public static function addIntegratedData($request){
        $integrated = Integrated::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$integrated->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 400, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $integrated->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$integrated->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 400, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $integrated->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$integrated->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(500, 500, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $integrated->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$integrated->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(800, 400, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $integrated->image4 = $imageName;
        }
        $integrated->title1 = $request->title1;
        $integrated->title2 = $request->title2;
        $integrated->m_title = $request->m_title;
        $integrated->m_des = $request->m_des;
        $integrated->s_title1 = $request->s_title1;
        $integrated->s_des1 = $request->s_des1;
        $integrated->s_title2 = $request->s_title2;
        $integrated->s_des2 = $request->s_des2;
        $integrated->s_title3 = $request->s_title3;
        $integrated->s_des3 = $request->s_des3;
        $integrated->s_title4 = $request->s_title4;
        $integrated->s_des4 = $request->s_des4;
        $integrated->s_title5 = $request->s_title5;
        $integrated->s_des5 = $request->s_des5;
        $integrated->save();
    }
}
