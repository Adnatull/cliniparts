<?php

namespace App;
use Auth;
use Image;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $guarded = [];

    public function getCategory(){
        return $this->belongsTo(NewsCategory::class, 'category_id', 'id');
    }
    public function authorName(){
        return $this->belongsTo(Admin::class, 'author_id', 'id');
    }

    public static function addNewsData($request){
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/News/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
        }
        News::create([
            'category_id' => $request->category_id,
            'title' => $request->title,
            'author_id' => Auth::guard('admin')->user()->id,
            'image' => $request->hasFile('image') ? $imageName : null,
            'des' => $request->des,
            'status' => $request->status,
        ]);
    }
    public static function updateNewsData($request){
        $news = News::find($request->id);
        if ($request->file('image')){
            @unlink('assets/backend/images/News/'.$news->image);
            $image = $request->file('image');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/News/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $news->image = $imageName;
        }
        $news->category_id = $request->category_id;
        $news->title = $request->title;
        $news->des = $request->des;
        $news->status = $request->status;
        $news->save();
    }
    public static function deleteNewsData($request){
        $news = News::find($request->id);
        @unlink('assets/backend/images/News/'. $news->image);
        $news->delete();
    }
}
