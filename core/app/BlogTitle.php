<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class BlogTitle extends Model
{
    public static function addBlogData($request){
        $title = BlogTitle::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/Blog/'.$title->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/Blog/'.$imageName;
            Image::make($image)->resize(400, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $title->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/Blog/'.$title->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/Blog/'.$imageName;
            Image::make($image)->resize(400, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $title->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/Blog/'.$title->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/Blog/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $title->image3 = $imageName;
        }
        $title->title1 = $request->title1;
        $title->title2 = $request->title2;
        $title->title3 = $request->title3;
        $title->save();
    }
}
