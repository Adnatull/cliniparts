<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $guarded = ['id'];

    public static function addBlogCategoryData($request){
        BlogCategory::create([
            'name' => $request->name,
            'status' => $request->status,
        ]);
    }
    public static function updateBlogCategoryData($request){
        $category = BlogCategory::find($request->id);
        $category->name = $request->name;
        $category->status = $request->status;
        $category->save();
    }
    public static function deleteBlogCategoryData($request){
        $category = BlogCategory::find($request->id);
        $category->delete();
    }
}
