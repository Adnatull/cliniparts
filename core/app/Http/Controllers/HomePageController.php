<?php

namespace App\Http\Controllers;

use App\HomeImage;
use App\HomeSection;
use App\HomeText;
use App\PopUp;
use App\Slider;
use Illuminate\Http\Request;
use Image;

class HomePageController extends Controller
{
    //Home Slider
    public function addSlider(){
        $sliders = Slider::orderBy('id', 'desc')->get();
        return view('backend.HomePage.add-slider', compact('sliders'));
    }
    public function saveSlider(Request $request){
        $this->validate($request,[
            'image' => 'required | mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Slider::addSliderData($request);
        return back()->withSuccess('Save Successful')->withInput();
    }
    public function updateSlider(Request $request){
        $this->validate($request,[
            'image' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Slider::updateSliderData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteSlider(Request $request){
        Slider::deleteSliderData($request);
        return back()->withSuccess('Delete Successful');
    }

    //Home Text
    public function addHomeText(){
        $homeText = HomeText::first();
        return view('backend.HomePage.add-home-text', compact('homeText'));
    }
    public function updateHomeText(Request $request){
        HomeText::addHomeTextData($request);
        return back()->withSuccess('Update Successful');
    }

    //Home Section
    public function addHomeSection(){
        $sections = HomeSection::orderBy('id', 'desc')->get();
        return view('backend.HomePage.add-home-section', compact('sections'));
    }
    public function saveHomeSection(Request $request){
        $this->validate($request,[
            'image' => 'required | mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'title' => 'required',
        ]);
        HomeSection::addHomeSectionData($request);
        return back()->withSuccess('Save Successful')->withInput();
    }
    public function updateHomeSection(Request $request){
        $this->validate($request,[
            'image' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'title' => 'required',
        ]);
        HomeSection::updateHomeSectionData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteHomeSection(Request $request){
        HomeSection::deleteHomeSectionData($request);
        return back()->withSuccess('Delete Successful');
    }

    //Home Subscribe Pop Up
    public function addHomePopUp(){
        $popup = PopUp::first();
        return view('backend.HomePage.add-popup', compact('popup'));
    }
    public function updateHomePopUp(Request $request){
        $this->validate($request,[
            'title1' => 'required',
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        PopUp::addHomePopUpData($request);
        return back()->withSuccess('Update Successful');
    }

    //Home Image
    public function addHomeImage(){
        $home = HomeImage::first();
        return view('backend.HomePage.add-home-image', compact('home'));
    }
    public function updateHomeImage(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image5' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image6' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image7' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image8' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image9' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image10' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image11' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image12' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image13' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image14' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        HomeImage::addHomeImageData($request);
        return back()->withSuccess('Update Successful');
    }
}
