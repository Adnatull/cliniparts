<?php

namespace App\Http\Controllers;

use App\AboutUs;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function addAboutUs(){
        $about = AboutUs::first();
        return view('backend.about.add-about', compact('about'));
    }
    public function updateAboutUs(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image5' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image6' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image7' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        AboutUs::updateAboutUsData($request);
        return back()->withSuccess('Update Successful');
    }
}
