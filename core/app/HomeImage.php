<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class HomeImage extends Model
{
    public static function addHomeImageData($request){
        $home = HomeImage::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/HomePage/'.$home->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/HomePage/'.$home->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->save($directory, $imageName);
//            Image::make($image)->resize(500, 500, function($constraint) { $constraint->aspectRatio();
//            })->save($directory, $imageName);
            $home->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/HomePage/'.$home->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(700, 500, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/HomePage/'.$home->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image4 = $imageName;
        }
        if ($request->file('image5')){
            @unlink('assets/backend/images/HomePage/'.$home->image5);
            $image = $request->file('image5');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(800, 500, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image5 = $imageName;
        }
        if ($request->file('image6')){
            @unlink('assets/backend/images/HomePage/'.$home->image6);
            $image = $request->file('image6');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image6 = $imageName;
        }
        if ($request->file('image7')){
            @unlink('assets/backend/images/HomePage/'.$home->image7);
            $image = $request->file('image7');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image7 = $imageName;
        }
        if ($request->file('image8')){
            @unlink('assets/backend/images/HomePage/'.$home->image8);
            $image = $request->file('image8');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image8 = $imageName;
        }
        if ($request->file('image9')){
            @unlink('assets/backend/images/HomePage/'.$home->image9);
            $image = $request->file('image9');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image9 = $imageName;
        }
        if ($request->file('image10')){
            @unlink('assets/backend/images/HomePage/'.$home->image10);
            $image = $request->file('image10');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image10 = $imageName;
        }
        if ($request->file('image11')){
            @unlink('assets/backend/images/HomePage/'.$home->image11);
            $image = $request->file('image11');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image11 = $imageName;
        }
        if ($request->file('image12')){
            @unlink('assets/backend/images/HomePage/'.$home->image12);
            $image = $request->file('image12');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image12 = $imageName;
        }
        if ($request->file('image13')){
            @unlink('assets/backend/images/HomePage/'.$home->image13);
            $image = $request->file('image13');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image13 = $imageName;
        }
        if ($request->file('image14')){
            @unlink('assets/backend/images/HomePage/'.$home->image14);
            $image = $request->file('image14');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $home->image14 = $imageName;
        }
        $home->save();
    }
}
