<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class PopUp extends Model
{
    public static function addHomePopUpData($request){
        $popup = PopUp::first();
        if ($request->file('image')){
            @unlink('assets/backend/images/HomePage/'.$popup->image);
            $image = $request->file('image');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(500, 500, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $popup->image = $imageName;
        }
        $popup->title1 = $request->title1;
        $popup->title2 = $request->title2;
        $popup->place = $request->place;
        $popup->button = $request->button;
        $popup->des = $request->des;
        $popup->save();
    }
}
