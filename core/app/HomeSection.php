<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class HomeSection extends Model
{
    protected $guarded = ['id'];

    public static function addHomeSectionData($request){
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->save($directory, $imageName);
        }
        HomeSection::create([
            'title' => $request->title,
            'des' => $request->des,
            'image' => $request->hasFile('image') ? $imageName : null,
            'status' => $request->status,
        ]);
    }
    public static function updateHomeSectionData($request){
        $section = HomeSection::find($request->id);
        if ($request->file('image')){
            @unlink('assets/backend/images/HomePage/'.$section->image);
            $image = $request->file('image');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->save($directory, $imageName);
            $section->image = $imageName;
        }
        $section->title = $request->title;
        $section->des = $request->des;
        $section->save();
    }
    public static function deleteHomeSectionData($request){
        $section = HomeSection::find($request->id);
        @unlink('assets/backend/images/HomePage/'.$section->image);
        $section->delete();
    }
}
