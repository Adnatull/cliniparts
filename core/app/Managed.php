<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Managed extends Model
{
    public static function addManagedData($request){
        $managed = Managed::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$managed->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $managed->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$managed->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $managed->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$managed->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $managed->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$managed->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $managed->image4 = $imageName;
        }
        $managed->title1 = $request->title1;
        $managed->title2 = $request->title2;
        $managed->m_title = $request->m_title;
        $managed->m_des = $request->m_des;
        $managed->s_title1 = $request->s_title1;
        $managed->s_des1 = $request->s_des1;
        $managed->s_title2 = $request->s_title2;
        $managed->s_des2 = $request->s_des2;
        $managed->s_title3 = $request->s_title3;
        $managed->s_des3 = $request->s_des3;
        $managed->s_title4 = $request->s_title4;
        $managed->s_des4 = $request->s_des4;
        $managed->s_title5 = $request->s_title5;
        $managed->s_des5 = $request->s_des5;
        $managed->save();
    }
}
